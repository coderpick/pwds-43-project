<?php
include "layout/head.php";
?>

<body>

<!-- Page Wrapper -->
<div id="wrap" class="layout-1">

    <!-- Top bar -->

    <?php
    include "layout/top_bar.php";
    ?>
    <!-- Header -->
    <?php
    include "layout/header.php";
    ?>
    <!-- Content -->
    <div id="content">

        <section class="login-sec padding-top-30 padding-bottom-100">
            <div class="container">
                <div class="row">
                    <!-- Don’t have an Account? Register now -->
                    <?php
                    if (isset($_SESSION['is_login']) && $_SESSION['is_login'] ==true){?>
                        <script>window.location.href='index.php'</script>
                    <?php   }
                    ?>å
                    <div class="col-md-6 col-md-offset-3">
                        <h5>Don’t have an Account? Register now</h5>
                        <?php
                        if (isset($_POST['register'])){

                            $data = array(
                                'name'      => $helper->validate($_POST['name']),
                                'email'     => $helper->validate($_POST['email']),
                                'mobile'    => $helper->validate($_POST['mobile']),
                                'password'  => $helper->validate($_POST['password'])
                            );

                            if (empty($data['name'])){
                                $error['name'] ="Name is required";
                            }
                            if (empty($data['email'])){
                                $error['email'] ="Email is required";
                            }
                            if (empty($data['mobile'])){
                                $error['mobile'] ="Mobile is required";
                            }
                            if (empty($data['password'])){
                                $error['password'] ="Password is required";
                            }

                            if (empty($error['name']) && empty($error['email']) && empty($error['mobile']) && empty($error['password'])){
                                $customer->register($data);
                            }

                        }

                        ?>


                        <!-- FORM -->
                        <form action="" method="post">
                            <ul class="row">
                                <li class="col-sm-12">
                                    <label>Name
                                        <input type="text" class="form-control" name="name" placeholder="Enter name">
                                        <span class="text-danger"><?php echo $error['name']??''?></span>
                                    </label>
                                </li>
                                <li class="col-sm-12">
                                    <label>Email
                                        <input type="email" class="form-control" name="email" placeholder="Enter email">
                                        <span class="text-danger"><?php echo $error['email']??''?></span>
                                    </label>
                                </li>
                                <li class="col-sm-12">
                                    <label>Mobile
                                        <input type="text" class="form-control" name="mobile" placeholder="Enter mobile number">
                                        <span class="text-danger"><?php echo $error['mobile']??''?></span>
                                    </label>
                                </li>
                                <li class="col-sm-12">
                                    <label>Password
                                        <input type="password" class="form-control" name="password" placeholder="Enter password">
                                        <span class="text-danger"><?php echo $error['password']??''?></span>
                                    </label>
                                </li>
                                <li class="col-sm-6 text-left">
                                    <button type="submit" name="register" class="btn-round">Register</button>
                                </li>
                                <li class="col-sm-6">
                                    Do you have already register? Please click here <strong><a style="color: #2196f3" href="login.php">Login</a></strong>
                                </li>
                            </ul>
                        </form>

                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- End Content -->
    <!-- Footer -->
    <?php include "layout/footer.php"; ?>
    <!-- End Footer -->

    <!-- GO TO TOP  -->
    <a href="index.html#" class="cd-top"><i class="fa fa-angle-up"></i></a>
    <!-- GO TO TOP End -->
</div>
<!-- End Page Wrapper -->

<!-- JavaScripts -->
<?php include "layout/_script.php"; ?>
<!-- page related jquery plugin load here...-->


<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<?php include "layout/_script_activate.php"; ?>

<!-- custom js -->

</body>

</html>