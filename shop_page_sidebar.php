<div class="shop-side-bar">

<!-- Featured Brands -->
<h6>Featured Brands</h6>
<div class="checkbox-primary">
    <ul>
        <?php
        $brands = $home->getAllBrand();
        if ($brands != null) {
            foreach ($brands as $brand) { ?>
                <li>
                    <a href="shop.php?brand=<?php echo $brand->slug ?>"><?php echo $brand->brand_name;?></a>
            
                </li>

        <?php     }
        }
        ?>
    </ul>
</div>

<!-- Categories -->
<h6>Categories</h6>
<div class="checkbox checkbox-primary">
    <ul>
        <?php
        $categories = $category->index();
        if ($categories != null) {
            foreach ($categories as $category) { ?>
                <li>
                <a href="shop.php?category=<?php echo $category->id ?>"><?php echo $category->name ?></a>

                </li>

        <?php     }
        }
        ?>
    </ul>
</div>

<!-- Categories -->

</div>