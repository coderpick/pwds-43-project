<header class="header-style-3">
    <div class="container">
        <div class="logo"> <a href="index.php"><img src="images/logo-3.png" alt=""></a> </div>

        <!-- Nav Header -->
        <nav class="navbar ownmenu">

            <!-- Categories -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-open-btn" aria-expanded="false"> <span><i class="fa fa-navicon"></i></span> </button>
            </div>

            <!-- NAV -->
            <div class="collapse navbar-collapse" id="nav-open-btn">
                <ul class="nav">
                    <li class="active"> <a href="index.php">Home </a></li>
                    <li class=""> <a href="shop.php">Shop </a></li>
                    <li class=""> <a href="index.php">About </a></li>
                    <li class=""> <a href="index.php">Contact </a></li>
                   <?php if (isset($_SESSION['is_login']) && $_SESSION['is_login'] ==true){?>
                    <li class=""> <a href="order.php">My Orders </a></li>
                    <?php
                       }
                    ?>

                </ul>
            </div>
        </nav>

        <!-- Cart Part -->
        <ul class="nav navbar-right cart-pop">
            <li class="dropdown">
                <a href="index-3.html#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="itm-cont">
                        <?php
                        if(isset( $_SESSION['cart'])){
                            echo count($_SESSION['cart']);
                        }else{
                            echo "0";
                        }
                        ?></span>
                    <i class="flaticon-shopping-bag"></i>
                    <strong>My Cart</strong> <br>
                    <span>
                        <?php
                        if(isset( $_SESSION['cart'])){
                            echo count($_SESSION['cart']);
                        }else{
                            echo "0";
                        }
                        ?> item(s)</span></a>
                <ul class="dropdown-menu">
                    <?php
                    if (isset($_SESSION['cart'])){
                        foreach ($_SESSION['cart'] as $item){?>
                            <li>
                                <div class="media-left"> <a href="index-3.html#." class="thumb"> <img src="admin/<?php echo $item['image'];?>" class="img-responsive" alt=""> </a> </div>
                                <div class="media-body"> <a href="index-3.html#." class="tittle"><?php echo $item['productName'];?></a> <span><?php echo $item['price'];?> x <?php echo $item['qty'];?></span> </div>
                            </li>
                        <?php         }
                    }
                    ?>


                    <li class="btn-cart"> <a href="cart.php" class="btn-round">View Cart</a> </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="nav-uder-bar">
        <div class="container">

            <!-- Categories -->
            <div class="cate-lst"> <a data-toggle="collapse" class="cate-style" href="index-3.html#cater"><i class="fa fa-list-ul"></i> Our Categories </a>
                <div class="cate-bar-in">
                    <div id="cater" class="collapse ">
                        <ul>
                            <?php
                            $categories = $category->index();
                            if ($categories != null) {
                                foreach ($categories as $categoryItem) {
                                    $sub_categories = $category->getSubCategory($categoryItem->id);
                                    if ($sub_categories != null) { ?>
                                        <li class="sub-menu"><a href="shop.php?category=<?php echo $categoryItem->id; ?>"><?php echo $categoryItem->name ?></a>
                                            <ul>
                                                <?php
                                                foreach ($sub_categories as $sub_category) {?>
                                                    <li><a href="shop.php?category=<?php echo $categoryItem->id; ?>&subcategory=<?php echo $sub_category->id; ?>"><?php echo $sub_category->name; ?></a></li>
                                                <?php }
                                                ?>
                                            </ul>
                                        </li>
                                    <?php  } else { ?>

                                        <li><a href="shop.php?category=<?php echo $categoryItem->id; ?>"><?php echo $categoryItem->name ?></a></li>
                                        <?php
                                    }
                                }
                            }
                            ?>


                            <!--  <li class="sub-menu"><a href="index-3.html#."> Cell Phones & Accessories</a>
                                <ul>
                                  <li><a href="index-3.html#."> TV & Video</a></li>
                                  <li><a href="index-3.html#."> Camera, Photo & Video</a></li>
                                  <li><a href="index-3.html#."> Cell Phones & Accessories</a>
                                </ul>
                              </li>-->


                        </ul>
                    </div>
                </div>
            </div>

            <!-- search -->
            <div class="search-cate">
                <select class="selectpicker">
                    <option> All Categories</option>
                    <?php
                    $categories = $category->index();
                    if ($categories != null) {
                        foreach ($categories as $categoryItem) {  ?>
                            <option value="<?php echo $categoryItem->id; ?>"><?php echo $categoryItem->name; ?></option>
                        <?php    }
                    }

                    ?>


                </select>
                <input type="search" placeholder="Search entire store here...">
                <button class="submit" type="submit"><i class="icon-magnifier"></i></button>
            </div>
            <!-- NAV RIGHT -->
            <div class="nav-right"> <span class="call-mun"><i class="fa fa-phone"></i> <strong>Hotline:</strong> (+100) 123 456 7890</span> </div>
        </div>
    </div>
</header>