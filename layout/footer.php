 <footer>
        <div class="container">

            <!-- Footer Upside Links -->
            <div class="foot-link">
                <ul>
                    <li><a href="index.html#."> About us </a></li>
                    <li><a href="index.html#."> Customer Service </a></li>
                    <li><a href="index.html#."> Privacy Policy </a></li>
                    <li><a href="index.html#."> Site Map </a></li>
                    <li><a href="index.html#."> Search Terms </a></li>
                    <li><a href="index.html#."> Advanced Search </a></li>
                    <li><a href="index.html#."> Orders and Returns </a></li>
                    <li><a href="index.html#."> Contact Us</a></li>
                </ul>
            </div>
            <div class="row">

                <!-- Contact -->
                <div class="col-md-4">
                    <h4>Contact SmartTech!</h4>
                    <p>Address: 45 Grand Central Terminal New York, NY 1017
                        United State USA</p>
                    <p>Phone: (+100) 123 456 7890</p>
                    <p>Email: Support@smarttech.com</p>
                    <div class="social-links"> <a href="index.html#."><i class="fa fa-facebook"></i></a> <a href="index.html#."><i class="fa fa-twitter"></i></a> <a href="index.html#."><i class="fa fa-linkedin"></i></a> <a href="index.html#."><i class="fa fa-pinterest"></i></a> <a href="index.html#."><i class="fa fa-instagram"></i></a> <a href="index.html#."><i class="fa fa-google"></i></a> </div>
                </div>

                <!-- Categories -->
                <div class="col-md-3">
                    <h4>Categories</h4>
                    <ul class="links-footer">
                        <li><a href="index.html#.">Home Audio & Theater</a></li>
                        <li><a href="index.html#."> TV & Video</a></li>
                        <li><a href="index.html#."> Camera, Photo & Video</a></li>
                        <li><a href="index.html#."> Cell Phones & Accessories</a></li>
                        <li><a href="index.html#."> Headphones</a></li>
                        <li><a href="index.html#."> Video Games</a></li>
                        <li><a href="index.html#."> Bluetooth & Wireless</a></li>
                    </ul>
                </div>

                <!-- Categories -->
                <div class="col-md-3">
                    <h4>Customer Services</h4>
                    <ul class="links-footer">
                        <li><a href="index.html#.">Shipping & Returns</a></li>
                        <li><a href="index.html#."> Secure Shopping</a></li>
                        <li><a href="index.html#."> International Shipping</a></li>
                        <li><a href="index.html#."> Affiliates</a></li>
                        <li><a href="index.html#."> Contact </a></li>
                    </ul>
                </div>

                <!-- Categories -->
                <div class="col-md-2">
                    <h4>Information</h4>
                    <ul class="links-footer">
                        <li><a href="index.html#.">Our Blog</a></li>
                        <li><a href="index.html#."> About Our Shop</a></li>
                        <li><a href="index.html#."> Secure Shopping</a></li>
                        <li><a href="index.html#."> Delivery infomation</a></li>
                        <li><a href="index.html#."> Store Locations</a></li>
                        <li><a href="index.html#."> FAQs</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- Rights -->
    <div class="rights">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <p>Copyright © 2017 <a href="index.html#." class="ri-li"> SmartTech </a>HTML5 template. All rights reserved</p>
                </div>
                <div class="col-sm-6 text-right"> <img src="images/card-icon.png" alt=""> </div>
            </div>
        </div>
    </div>
