<?php
include "layout/head.php";
?>

<body>

    <!-- Page Wrapper -->
    <div id="wrap" class="layout-1">
        <!-- Top bar -->
        <?php
        include "layout/top_bar.php";
        ?>
        <!-- Header -->
        <?php
        include "layout/header.php";
        ?>
        <!-- Content -->
        <div id="content">
            <?php
            if ($customer->isLoggedIn() != true) {
                echo '<script>window.location.href="login.php"</script>';
            }
            ?>
            <!-- Shopping Cart -->
            <section class="padding-bottom-60 margin-top-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="order-summery">

                                <?php

                                if (isset($_GET['id'])) {
                                    $orderId = base64_decode($_GET['id']);

                                    $customer = $order->customerOrderInfo($orderId);
                                }

                                ?>
                                <div id="invoice">
                                    <div class="text-center">
                                        <h3>SmartShop.com</h3>
                                        <hr>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h5> Billing Address:</h5>
                                            <p> To: <?php echo $customer->name ?? ''; ?></p>
                                            <p><strong>Phone: <?php echo $customer->mobile ?? ''; ?></strong></p>
                                            <p>Address: <?php echo $customer->shipping_add ?? ''; ?></p>

                                        </div>
                                        <div class="col-md-4 col-md-offset-4">
                                            <h5>Invoice: #<?php echo $customer->order_number ?? ""; ?></h5>
                                            <p>Order Date: <?php echo $customer->order_date ?? ""; ?></p>
                                            <p><strong>Status: <?php echo $customer->status ?? ""; ?></strong></p>
                                        </div>
                                    </div>

                                    <table class="table table-bordered">
                                        <tr>
                                            <th>#</th>
                                            <th>Product</th>
                                            <th>Price</th>
                                            <th>Qty</th>
                                            <th>Total</th>
                                        </tr>
                                        <?php
                                        $orderProducts = $order->orderProduct($orderId);
                                        if ($orderProducts != null) {
                                            $totalPrice = 0;
                                            foreach ($orderProducts as $key => $orderProduct) { ?>
                                                <tr>
                                                    <td><?php echo $key + 1; ?></td>
                                                    <td><?php echo $orderProduct->product_name; ?></td>
                                                    <td><?php echo $orderProduct->price; ?></td>
                                                    <td><?php echo $orderProduct->quantity; ?></td>
                                                    <td><?php
                                                        $total = $orderProduct->price * $orderProduct->quantity;
                                                        echo $total;
                                                        $totalPrice += $total;
                                                        ?></td>
                                                </tr>

                                        <?php         }
                                        }
                                        ?>
                                        <tr>
                                            <td class="text-right" colspan="4">Sub Total</td>
                                            <td><?php echo $totalPrice ?? ''; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="text-right" colspan="4">Delivery Charge</td>
                                            <td><?php echo $customer->shaping_charge??0 ?></td>
                                        </tr>
                                        <tr>
                                            <td class="text-right" colspan="4">Grand Total</td>
                                            <td><?php echo $totalPrice + $customer->shaping_charge ?? ''; ?></td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </div>
        <!-- End Content -->
        <!-- Footer -->
        <?php include "layout/footer.php"; ?>
        <!-- End Footer -->

        <!-- GO TO TOP  -->
        <a href="index.html#" class="cd-top"><i class="fa fa-angle-up"></i></a>
        <!-- GO TO TOP End -->
    </div>
    <!-- End Page Wrapper -->

    <!-- JavaScripts -->
    <?php include "layout/_script.php"; ?>
    <!-- page related jquery plugin load here...-->


    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <?php include "layout/_script_activate.php"; ?>

    <!-- custom js -->

</body>

</html>