<?php
include "layout/head.php";
?>

<body>

<!-- Page Wrapper -->
<div id="wrap" class="layout-1">
    <!-- Top bar -->
    <?php
    include "layout/top_bar.php";
    ?>
    <!-- Header -->
    <?php
    include "layout/header.php";
    ?>
    <!-- Content -->
    <div id="content">
        <?php
        if ( $customer->isLoggedIn() !=true){
            echo '<script>window.location.href="login.php"</script>';
        }
        ?>
        <!-- Shopping Cart -->
        <section class="padding-bottom-60 margin-top-20">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="customer-info">
                            <h5>Shipping Information</h5>
                            <hr>
                            <?php
                            /* update customer info */
                            if (isset($_POST['update'])){
                                $data =[
                                    'customerId' => $helper->validate($_POST['customerId']),
                                    'name'      => $helper->validate($_POST['name']),
                                    'email'     => $helper->validate($_POST['email']),
                                    'mobile'    => $helper->validate($_POST['mobile']),
                                    'shipping_address'  => $helper->validate($_POST['shipping_address'])
                                ];

                                if (empty($data['name'])){
                                    $error['name'] ="Name is required";
                                }
                                if (empty($data['email'])){
                                    $error['email'] ="Email is required";
                                }
                                if (empty($data['mobile'])){
                                    $error['mobile'] ="Mobile is required";
                                }
                                if (empty($data['shipping_address'])){
                                    $error['shipping_address'] ="Shipping address is required";
                                }
                                if (empty($error['name']) && empty($error['email']) && empty($error['mobile']) && empty($error['shipping_address'])){

                                    $customer->customerInfoUpdate($data);
                                }

                            }

                            /*get customer info*/
                            if (isset($_SESSION['id'])){
                                $customerId = $_SESSION['id'];
                                $row = $customer->customerInfo($customerId);
                            }
                            ?>
                            <form action="" method="post">
                                <input type="hidden"  name="customerId"  value="<?php echo  $row->id??''; ?>">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Name</label>
                                    <input type="text" class="form-control" name="name" id="exampleInputEmail1" value="<?php echo  $row->name??''; ?>"  placeholder="Name">
                                    <span class="text-danger"><?php echo $error['name']??''?></span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" name="email" id="exampleInputEmail1" readonly value="<?php echo  $row->email??''; ?>"   placeholder="Email">
                                    <span class="text-danger"><?php echo $error['email']??''?></span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mobile</label>
                                    <input type="text" class="form-control" name="mobile" id="exampleInputEmail1" value="<?php echo  $row->mobile??''; ?>"   placeholder="Mobile">
                                    <span class="text-danger"><?php echo $error['mobile']??''?></span>
                                </div>
                                <div class="form-group">
                                    <label for="shipping_address">Shipping address</label>
                                    <textarea class="form-control" rows="5" placeholder="Enter your shipping address" name="shipping_address" id="shipping_address" ><?php echo $row->shipping_add; ?></textarea>
                                    <span class="text-danger"><?php echo $error['shipping_address']??''?></span>
                                </div>

                                <button type="submit" name="update"  class="btn btn-primary">Update</button>

                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="order-summery">
                            <h5>Order Summery</h5>
                            <hr>
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th>Items</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-center">Total Price </th>
                                </tr>

                                <tbody>
                                <?php
                                if ( isset( $_SESSION['cart'] ) ) {
                                    $totalPrice = 0;
                                    foreach ( $_SESSION['cart'] as $item ) {?>
                                        <!-- Item Cart -->
                                        <tr>
                                            <td><div class="media">
                                                    <div class="media-left"> <a href="#"> <img class="img-responsive" src="admin/<?php echo $item['image'];?>" alt="" > </a> </div>
                                                    <div class="media-body">
                                                        <p><?php echo $item['productName']; ?></p>
                                                    </div>
                                                </div></td>
                                            <td class="text-center"><?php echo $item['price'];?></td>
                                            <td class="text-center">
                                                <!-- Quantity -->
                                                <div class="quinty">
                                                    <span><?php echo $item['qty']; ?></span>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                $subTotal = $item['qty'] * $item['price'];
                                                echo $subTotal;
                                                $totalPrice += $subTotal;
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                <tr>
                                    <td colspan="3" class="text-right">Sub Total:</td>
                                    <td colspan="3" class="text-right"><?php echo $totalPrice??''; ?></td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-right">Shipping Charge:</td>
                                    <td colspan="3" class="text-right"><?php
                                        $shipping_charge = 30;
                                        echo $shipping_charge;
                                        ?></td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-right">Grand Total:</td>
                                    <td colspan="3" class="text-right"><?php echo $totalPrice + $shipping_charge; ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- submit order-->
                    <?php
                    if (isset($_POST['btnOrder'])){
                        $data['customerId'] = $_POST['customerId'];
                        $data['total_price'] = $_POST['total_price'];
                        $data['shipping_charge'] = $_POST['shipping_charge'];
                        $order->order($data);
                    }
                    ?>
                    <div class="col-sm-12 text-center">
                        <hr>
                        <?php
                        if ($row->shipping_add !=null){?>
                            <form action="" method="post">
                                <input type="hidden"  name="customerId"  value="<?php echo  $row->id??''; ?>">
                                <input type="hidden"  name="total_price"  value="<?php echo  $totalPrice??''; ?>">
                                <input type="hidden"  name="shipping_charge"  value="<?php echo  $shipping_charge; ?>">
                                <button type="submit"  name="btnOrder" class="btn btn-danger btn-lg">Oder Now</button>
                            </form>
                        <?php  }else{?>
                            <a href="javascript: void(0);" disabled class="btn btn-danger btn-lg" title="Please enter shipping address first">Oder Now</a>
                        <?php    }
                        ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- End Content -->
    <!-- Footer -->
    <?php include "layout/footer.php"; ?>
    <!-- End Footer -->

    <!-- GO TO TOP  -->
    <a href="index.html#" class="cd-top"><i class="fa fa-angle-up"></i></a>
    <!-- GO TO TOP End -->
</div>
<!-- End Page Wrapper -->

<!-- JavaScripts -->
<?php include "layout/_script.php"; ?>
<!-- page related jquery plugin load here...-->


<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<?php include "layout/_script_activate.php"; ?>

<!-- custom js -->

</body>

</html>