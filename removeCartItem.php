<?php
session_start();
if(isset($_GET['productId'])){
    $productId      = $_GET['productId'];
    if ( isset( $_SESSION['cart'] ) ) {
        foreach ( $_SESSION["cart"] as $keys => $values ) {
            if ( $values["product_id"] == $productId ) {
                unset( $_SESSION["cart"][$keys] );
                $_SESSION['toastr'] = array(
                    'type'      => 'success', // or 'success' or 'info' or 'warning'
                    'message'   => 'Product successfully remove from your cart',
                    'title'     => 'Success'
                );
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }
        }

    }
}

?>