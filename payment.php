<?php
include "layout/head.php";
?>

<body>

<!-- Page Wrapper -->
<div id="wrap" class="layout-1">
    <!-- Top bar -->
    <?php
    include "layout/top_bar.php";
    ?>
    <!-- Header -->
    <?php
    include "layout/header.php";
    ?>
    <!-- Content -->
    <div id="content">
        <!-- Shopping Cart -->
        <section class="shopping-cart padding-bottom-60 margin-top-20">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Choose payment options</h3>
                            </div>
                            <div class="panel-body text-center" style="padding:50px 0">
                                <a href="paymentoffline.php" class="btn btn-primary btn-lg" role="button">Cash on Delivery</a>
                                <a href="paymentonline.php" class="btn btn-info btn-lg " role="button">Online Payment</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- End Content -->
    <!-- Footer -->
    <?php include "layout/footer.php"; ?>
    <!-- End Footer -->

    <!-- GO TO TOP  -->
    <a href="index.html#" class="cd-top"><i class="fa fa-angle-up"></i></a>
    <!-- GO TO TOP End -->
</div>
<!-- End Page Wrapper -->

<!-- JavaScripts -->
<?php include "layout/_script.php"; ?>
<!-- page related jquery plugin load here...-->


<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<?php include "layout/_script_activate.php"; ?>

<!-- custom js -->

</body>

</html>