<?php
include "layout/head.php";
?>

<body>

<!-- Page Wrapper -->
<div id="wrap" class="layout-1">

    <!-- Top bar -->

    <?php
    include "layout/top_bar.php";
    ?>
    <!-- Header -->
    <?php
    include "layout/header.php";
    ?>
    <!-- Content -->
    <div id="content">

        <section class="login-sec padding-top-30 padding-bottom-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">

                            <?php
                            if (isset($_SESSION['is_login']) && $_SESSION['is_login'] ==true){?>
                                    <script>window.location.href='index.php'</script>
                         <?php   }
                            ?>

                        <!-- Login Your Account -->
                        <h5>Login Your Account</h5>
                        <!-- FORM -->
                        <?php
                        if (isset($_POST['login'])){
                            $data =[
                                'email'     => $helper->validate($_POST['email']),
                                'password'  => $helper->validate($_POST['password'])
                            ];
                            if (empty($data['email'])){
                                $error['email'] ="Email is required";
                            }
                            if (empty($data['password'])){
                                $error['password'] ="Password is required";
                            }

                            if (empty($error['email']) && empty($error['password'])){
                                $customer->login($data);
                            }
                        }

                        ?>
                        <form action="" method="post">
                            <ul class="row">
                                <li class="col-sm-12">
                                    <label>Email
                                        <input type="text" class="form-control" name="email" placeholder="Enter your email">
                                        <span class="text-danger"><?php echo $error['email']??''?></span>
                                    </label>
                                </li>
                                <li class="col-sm-12">
                                    <label>Password
                                        <input type="password" class="form-control" name="password" placeholder="Enter your password">
                                        <span class="text-danger"><?php echo $error['password']??''?></span>
                                    </label>
                                </li>
                                <li class="col-sm-6"> <a href="LoginForm.html#">Forgot your password?</a> </li>
                                <li class="col-sm-6">
                                    Click here if you do not have an account
                                    <strong>  <a href="signup.php" style="color: #2196f3" >Sign up</a></strong>
                                </li>
                                <li class="col-sm-12 text-left">
                                    <button type="submit" name="login" class="btn-round">Login</button>
                                </li>
                            </ul>
                        </form>
                    </div>

                </div>
            </div>
        </section>
    </div>
    <!-- End Content -->
    <!-- Footer -->
    <?php include "layout/footer.php"; ?>
    <!-- End Footer -->

    <!-- GO TO TOP  -->
    <a href="index.html#" class="cd-top"><i class="fa fa-angle-up"></i></a>
    <!-- GO TO TOP End -->
</div>
<!-- End Page Wrapper -->

<!-- JavaScripts -->
<?php include "layout/_script.php"; ?>
<!-- page related jquery plugin load here...-->


<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<?php include "layout/_script_activate.php"; ?>

<!-- custom js -->

</body>

</html>