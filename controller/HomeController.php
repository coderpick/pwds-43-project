<?php


class HomeController
{
  private  $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getSliders()
    {
        $sql = "SELECT * FROM slider WHERE status=true";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function getAllBrand()
    {
         $sql = "SELECT * FROM brand WHERE status=true";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

}