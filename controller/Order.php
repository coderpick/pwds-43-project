<?php


class Order
{
    private  $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function order($data)
    {
        $customerId  = $data['customerId'];
        $total_price = $data['total_price'];
        $shippingCharge  = $data['shipping_charge'];
        $orderNumber = date('Ymd').rand(0000, 999999);
        $orderDate   = date('Y-m-d h:i:s');

        if(isset( $_SESSION['cart'])){
            $totalItem = count($_SESSION['cart']);
        }

        try {

            $sql = "INSERT INTO orders(customer_id, order_number, order_date, total_item, total_amount, shaping_charge,payment_type) VALUES(:customerId,:oderNumber,:orderDate,:totalItem,:totalAmount,:shippingCharge,:payment_type)";
            $this->db->query($sql);
            $this->db->bind(":customerId",$customerId);
            $this->db->bind(":oderNumber",$orderNumber);
            $this->db->bind(":orderDate",$orderDate);
            $this->db->bind(":totalItem",$totalItem);
            $this->db->bind(":totalAmount",$total_price);
            $this->db->bind(":shippingCharge",$shippingCharge);
            $this->db->bind(":payment_type",'COD');
            $this->db->execute();
            $lastId = $this->db->lastInsertId();
            if ($lastId){
                if(isset( $_SESSION['cart'])) {
                    foreach ($_SESSION['cart'] as $item) {

                        $subTotal = $item['qty'] * $item['price'];

                        $sql ="INSERT INTO order_product(order_id, product_name, price, quantity, sub_total) VALUES (:orderId,:product_name,:price,:quantity,:sub_total)";
                        $this->db->query($sql);
                        $this->db->bind(":orderId",$lastId);
                        $this->db->bind(":product_name",$item['productName']);
                        $this->db->bind(":price",$item['price']);
                        $this->db->bind(":quantity",$item['qty']);
                        $this->db->bind(":sub_total", $subTotal);
                        $this->db->execute();
                    }
                }

                $_SESSION['toastr'] = array(
                    'type'      => 'success', // or 'success' or 'info' or 'warning'
                    'message'   => 'Your Order Placed Successfully',
                    'title'     => 'Success'
                );
                unset($_SESSION['cart']);
                // echo '<script>window.location.href="index.php"</script>';
            }else{
                $_SESSION['toastr'] = array(
                    'type'      => 'warning', // or 'success' or 'info' or 'warning'
                    'message'   => 'Your Order Placed Failed Try Again',
                    'title'     => 'Error'
                );
                echo '<script>window.location.href="index.php"</script>';
            }


        }catch (PDOException $e){
            echo $e->getMessage();
        }


    }

    public function getLatestOrder($id){

        $sql ="SELECT * FROM orders WHERE customer_id=:customerId ORDER BY id DESC LIMIT 1";
        $this->db->query($sql);
        $this->db->bind(":customerId",$id);
        return $this->db->single();

    }


public function orderUpdate($orderId,$data)
    {

       $sql ="UPDATE orders SET transaction_id=:transaction_id, paid_amount=:paid_amount, currency=:currency,payment_type=:payment_type,payment_status=:payment_status WHERE id=:orderId";
       $this->db->query($sql);
       $this->db->bind(":orderId",$orderId);
       $this->db->bind(":transaction_id",$data['tran_id']);
       $this->db->bind(":paid_amount",$data['paid_amount']);
       $this->db->bind(":currency",$data['currency_type']);
       $this->db->bind(":payment_type", 'Online');
       $this->db->bind(":payment_status", 'Paid');
       if ($this->db->execute()){
          return true;
       }else{
         return false;
       }

    }


      public function customerOrder($id){

        $sql = "SELECT * FROM orders WHERE customer_id=:customerId ORDER BY id DESC";
        $this->db->query($sql);
        $this->db->bind(":customerId",$id);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function customerOrderInfo($id)
    {
        $sql = "SELECT orders.*,customers.name,customers.mobile,customers.shipping_add FROM orders INNER JOIN customers ON orders.customer_id = customers.id WHERE orders.id=:orderId";
        $this->db->query($sql);
        $this->db->bind(":orderId",$id);
       return $this->db->single();

    }

    public function orderProduct($id)
    {
        $sql = "SELECT * FROM order_product WHERE order_id=:orderId";
        $this->db->query($sql);
        $this->db->bind(":orderId",$id);
        $this->db->execute();
       return $this->db->resultSet();
    }

}