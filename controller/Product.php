<?php


class Product
{
    private $db;
    public function __construct()
    {
        $this->db = new Database();
    }

    public function getFeatureProducts()
    {
        $sql = "SELECT product.id,product.product_name,product.slug,product.feature_image,product.price,category.name AS categoryName FROM product INNER JOIN category ON product.category_id= category.id WHERE product.is_featured =true ORDER BY product.id";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function getAllProducts()
    {
        $sql = "SELECT product.id,product.product_name,product.slug,product.feature_image,product.price,product.discount,product.discount_price,category.name AS categoryName FROM product INNER JOIN category ON product.category_id= category.id WHERE product.status =true ORDER BY product.id";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }


    public function productDetails($slug)
    {
        $sql = "SELECT product.*,category.name as CategoryName FROM product INNER JOIN category ON product.category_id = category.id WHERE product.slug=:productSlug";
        $this->db->query($sql);
        $this->db->bind(":productSlug", $slug);
        $this->db->execute();
        return $this->db->single();
    }

    public function productImges($productId)
    {
        $sql = "SELECT * FROM product_images WHERE product_id=:productId";
        $this->db->query($sql);
        $this->db->bind(":productId", $productId);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function categoryProduct($categoryId)
    {
        $sql = "SELECT product.id,product.category_id,product.product_name,product.slug,product.feature_image,product.price,product.discount,product.discount_price,category.name AS categoryName FROM product INNER JOIN category ON product.category_id= category.id WHERE product.status =true AND product.category_id=:categoryId ORDER BY product.id";
        $this->db->query($sql);
        $this->db->bind(":categoryId", $categoryId);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function categoryAndSubCategoryProduct($categoryId, $subCategoryId)
    {
        $sql = "SELECT product.id,product.category_id,product.product_name,product.slug,product.feature_image,product.price,product.discount,product.discount_price,category.name AS categoryName FROM product INNER JOIN category ON product.category_id= category.id WHERE product.status =true AND product.category_id=:categoryId AND product.sub_category_id=:subCategoryId ORDER BY product.id";
        $this->db->query($sql);
        $this->db->bind(":categoryId", $categoryId);
        $this->db->bind(":subCategoryId", $subCategoryId);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function brandProduct($brandSlug)
    {
            // slect brand by requested slug

            $sql ="SELECT * FROM brand WHERE slug=:slug";
            $this->db->query($sql);
            $this->db->bind(':slug',$brandSlug);
            $this->db->execute();
            $brand = $this->db->single();

         if($brand){
            $sql = "SELECT product.id,product.category_id,product.product_name,product.slug,product.feature_image,product.price,product.discount,product.discount_price,category.name AS categoryName FROM product INNER JOIN category ON product.category_id= category.id WHERE product.status =true AND product.brand_id=:brandId ORDER BY product.id";
            $this->db->query($sql);
            $this->db->bind(":brandId", $brand->id);
            $this->db->execute();
            return $this->db->resultSet();
       }
    }
}
