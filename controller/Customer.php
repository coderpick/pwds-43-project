<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Customer
{
    use Notify;
    private  $db;

    public function __construct()
    {
        $this->db = new Database();
    }
    public function customerInfo($id)
    {
        $sql ="SELECT * FROM customers WHERE id=:customerId";
        $this->db->query($sql);
        $this->db->bind(":customerId",$id);
        $this->db->execute();
        return $this->db->single();
    }
        public function customerInfoUpdate($data)
    {
        $sql ="UPDATE customers SET name=:name,mobile=:mobile, shipping_add=:shippingAddress WHERE id=:customerId";
        $this->db->query($sql);
        $this->db->bind(":customerId",$data['customerId']);
        $this->db->bind(":name",$data['name']);
        $this->db->bind(":mobile",$data['mobile']);
        $this->db->bind(":shippingAddress",$data['shipping_address']);
        if ($this->db->execute()) {
            $_SESSION['toastr'] = array(
                'type'      => 'success', // or 'success' or 'info' or 'warning'
                'message'   => 'Your information update successfully',
                'title'     => 'Success'
            );
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        } else {
            $_SESSION['toastr'] = array(
                'type'      => 'warning', // or 'success' or 'info' or 'warning'
                'message'   => 'Information update failed',
                'title'     => 'Success'
            );
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
    }

    public function register($data)
    {
        try {
            /*verify code generate*/
            $code = md5(uniqid(rand()));
            $password = password_hash($data['password'],PASSWORD_DEFAULT);
            /*check customer if exists*/
            $sql ="SELECT * FROM customers WHERE email=:email";
            $this->db->query($sql);
            $this->db->bind(':email',$data['email']);
            $this->db->execute();
            if ($this->db->rowCount() > 0){
                echo 'Email already exists , Please Try another one';
            }else{
                $sql ="INSERT INTO customers(name,email,password,mobile,_token)VALUES(:name,:email,:password,:mobile,:token)";
                $this->db->query($sql);
                $this->db->bind(":name",$data['name']);
                $this->db->bind(":email",$data['email']);
                $this->db->bind(":password",$password);
                $this->db->bind(":mobile",$data['mobile']);
                $this->db->bind(":token",$code);
                $this->db->execute();
                $lastId = $this->db->lastInsertId();
                $key = base64_encode($lastId);
                $id = $key;
                $message = " Hello ".$data['name'].", <br /><br />
                        Welcome to SmartShop!<br/> To complete your registration  please , just click following link<br/>
                            <br /><br />
                      <a href='http://localhost/pwds-43-project/verify.php?id=$id&code=$code'>Click HERE to Activate</a>
                      <br /><br />
                      Thanks,";
                $subject = "Confirm Customer Registration";
                if ($this->send_mail($data['email'], $message, $subject))  {

                    echo "We've sent an email to".$data['email']."Please click on the confirmation link in the email to create your account.";
                }
            }


        }catch (PDOException $exception){
            echo $exception->getMessage();
        }
    }


    public function login($data)
    {
        $email    = $data['email'];
        $password = $data['password'];
        try
        {
            $sql ="SELECT * FROM customers WHERE email=:email";
            $this->db->query($sql);
            $this->db->bind(':email',$email);
            $row = $this->db->single();
            if ($this->db->rowCount() > 0){
                if ($row->is_verified =='Yes'){
                    if ( password_verify($password,$row->password) ) {
                        $_SESSION['is_login'] = true;
                        $_SESSION['id']       = $row->id;
                        $_SESSION['name']     = $row->name;
                        $_SESSION['mobile']   = $row->mobile;
                        $_SESSION['email']    = $row->email;
                        echo '<script>window.location.href="index.php"</script>';
                    }else{
                        echo "<div class='alert alert-danger alert-dismissible'>
                            <button class='close' data-dismiss='alert'>&times;</button>
                          <strong>Sorry !</strong> Password does match!</a>
                          </div>";
                    }
                } else{
                    echo "<div class='alert alert-danger alert-dismissible'>
                <button class='close' data-dismiss='alert'>&times;</button>
              <strong>Sorry !</strong> This Account is not Activated Go to your Inbox and Activate it!</a>
              </div>";
                }
            }else{
                echo "<div class='alert alert-danger alert-dismissible'>
                <button class='close' data-dismiss='alert'>&times;</button>
              <strong>Sorry !</strong> Email does match!</a>
              </div>";
            }

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

      public function isLoggedIn()
    {
        if ( $_SESSION['is_login'] == true){
            return true;
        }else{
            return false;
        }
    }


    public function logout()
    {
        session_destroy();
        echo '<script>window.location.href="index.php"</script>';
    }


        public function userVerify($id,$code)
    {


        $statusY = "Yes";
        $statusN = "No";
        $stmt = "SELECT id,is_verified FROM customers WHERE id=:uID AND _token=:code LIMIT 1";
        $this->db->query($stmt);
        $this->db->bind(":uID", $id);
        $this->db->bind(":code", $code);
        $this->db->execute();
        $row = $this->db->single();
        if ( $this->db->rowCount() > 0 ) {
            if ( $row->is_verified == $statusN ) {
                $stmt = "UPDATE customers SET is_verified=:status WHERE id=:uID";
                $this->db->query($stmt);
                $this->db->bind( ":status", $statusY );
                $this->db->bind( ":uID", $id );
                $this->db->execute();
                return "<div class='alert alert-success'>
                   <button class='close' data-dismiss='alert'>&times;</button>
                   <strong>WoW !</strong>  Your Account is Now Activated : <a href='login.php'>Login here</a>
                      </div>";
            } else {
                return "<div class='alert alert-error'>
               <button class='close' data-dismiss='alert'>&times;</button>
               <strong>Sorry !</strong>  Your Account is already Activated : <a href='login.php'>Login here</a>
                </div>";
            }
        } else {
            return "<div class='alert alert-error'>
              <button class='close' data-dismiss='alert'>&times;</button>
              <strong>Sorry !</strong>  No Account Found : <a href='login.php'>Signup here</a>
              </div>";
        }
    }



    private  function send_mail( $email, $message, $subject ) {
        require "./PHPMailer/src/Exception.php";
        require "./PHPMailer/src/PHPMailer.php";
        require "./PHPMailer/src/SMTP.php";
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "ssl";
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 465;
        $mail->AddAddress( $email );
        $mail->Username = ""; // your email address
        $mail->Password = "";// your email password
        $mail->SetFrom( 'hafizur@peoplentech.net', 'Eshop' );
        $mail->AddReplyTo( "hafizur@peoplentech.net", "Eshop" );
        $mail->Subject = $subject;
        $mail->MsgHTML( $message );
        $mail->Send();
    }
}