<?php


class Category
{
    private $db;
    public function __construct()
    {
        $this->db = new Database();
    }

    public function index()
    {
        $sql = "SELECT * FROM category WHERE status = true";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();


    }

    public function getSubCategory($id)
    {
        $sql = "SELECT * FROM sub_category WHERE category_id =:categoryId";
        $this->db->query($sql);
        $this->db->bind(":categoryId",$id);
        $this->db->execute();
        return $this->db->resultSet();
    }
}