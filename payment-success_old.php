<?php
include "layout/head.php";
?>

<body>

<!-- Page Wrapper -->
<div id="wrap" class="layout-1">
    <!-- Top bar -->
    <?php
    include "layout/top_bar.php";
    ?>
    <!-- Header -->
    <?php
    include "layout/header.php";
    ?>
    <!-- Content -->
    <div id="content">
        <!-- Shopping Cart -->
        <section class="shopping-cart padding-bottom-60 margin-top-20">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <?php

                        if (isset($_GET['id'])){
                            $customerId = base64_decode($_GET['id']);

                            //    get customer latest order
                            $getLatestOrder = $order->getLatestOrder($customerId);
                            $orderId        = $getLatestOrder->id;

                        }


                        $val_id=urlencode($_POST['val_id']);
                        $store_id=urlencode("smart6258f41568649");
                        $store_passwd=urlencode("smart6258f41568649@ssl");
                        $requested_url = ("https://sandbox.sslcommerz.com/validator/api/validationserverAPI.php?val_id=".$val_id."&store_id=".$store_id."&store_passwd=".$store_passwd."&v=1&format=json");

                        $handle = curl_init();
                        curl_setopt($handle, CURLOPT_URL, $requested_url);
                        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false); # IF YOU RUN FROM LOCAL PC
                        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false); # IF YOU RUN FROM LOCAL PC

                        $result = curl_exec($handle);

                        $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

                        if($code == 200 && !( curl_errno($handle)))
                        {

                            # TO CONVERT AS ARRAY
                            # $result = json_decode($result, true);
                            # $status = $result['status'];

                            # TO CONVERT AS OBJECT
                            $result = json_decode($result);

                          // print_r($result);

                            # TRANSACTION INFO
                            $status = $result->status;
                            $tran_date = $result->tran_date;
                            $tran_id = $result->tran_id;
                            $val_id = $result->val_id;
                            $amount = $result->amount;
                            $store_amount = $result->store_amount;
                            $bank_tran_id = $result->bank_tran_id;
                            $card_type = $result->card_type;
                            $currency_type = $result->currency_type;

                            $data =[
                                'tran_id'       => $tran_id,
                                'paid_amount'   => $amount,
                                'currency_type' => $currency_type,
                                'payment_type ' => 'Online',
                                'payment_status ' => 'Paid',
                            ];


                              $orderUpdate = $order->orderUpdate($orderId,$data);

                            # EMI INFO
                            $emi_instalment = $result->emi_instalment;
                            $emi_amount = $result->emi_amount;
                            $emi_description = $result->emi_description;
                            $emi_issuer = $result->emi_issuer;

                            # ISSUER INFO
                            $card_no = $result->card_no;
                            $card_issuer = $result->card_issuer;
                            $card_brand = $result->card_brand;
                            $card_issuer_country = $result->card_issuer_country;
                            $card_issuer_country_code = $result->card_issuer_country_code;

                            # API AUTHENTICATION
                            $APIConnect = $result->APIConnect;
                            $validated_on = $result->validated_on;
                            $gw_version = $result->gw_version;

                        } else {

                            echo "Failed to connect with SSLCOMMERZ";
                        }

                        ?>

                        <!--   Update order information-->



                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- End Content -->
    <!-- Footer -->
    <?php include "layout/footer.php"; ?>
    <!-- End Footer -->

    <!-- GO TO TOP  -->
    <a href="index.html#" class="cd-top"><i class="fa fa-angle-up"></i></a>
    <!-- GO TO TOP End -->
</div>
<!-- End Page Wrapper -->

<!-- JavaScripts -->
<?php include "layout/_script.php"; ?>
<!-- page related jquery plugin load here...-->


<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<?php include "layout/_script_activate.php"; ?>

<!-- custom js -->

</body>

</html>