<?php
include "layout/head.php";
?>

<body>

<!-- Page Wrapper -->
<div id="wrap" class="layout-1">

    <!-- Top bar -->

    <?php
    include "layout/top_bar.php";
    ?>
    <!-- Header -->
    <?php
    include "layout/header.php";
    ?>
    <!-- Content -->
    <div id="content">

      <section class="login-sec padding-top-30 padding-bottom-100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <?php
                        if ( empty( $_GET['id'] ) && empty( $_GET['code'] ) ) {
                            header("Location:index.php");
                        }

                        if ( isset( $_GET['id'] ) && isset( $_GET['code'] ) ) {
                            $id = base64_decode( $_GET['id'] );
                            $code = $_GET['code'];
                            $user_verify = $customer->userVerify($id,$code);
                            if (isset($user_verify)){
                                echo $user_verify;
                            }
                        }

                        ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- End Content -->
    <!-- Footer -->
    <?php include "layout/footer.php"; ?>
    <!-- End Footer -->

    <!-- GO TO TOP  -->
    <a href="index.html#" class="cd-top"><i class="fa fa-angle-up"></i></a>
    <!-- GO TO TOP End -->
</div>
<!-- End Page Wrapper -->

<!-- JavaScripts -->
<?php include "layout/_script.php"; ?>
<!-- page related jquery plugin load here...-->


<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<?php include "layout/_script_activate.php"; ?>

<!-- custom js -->

</body>

</html>