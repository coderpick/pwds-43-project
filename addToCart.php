<?php
session_start();
$filepath = realpath(dirname(__FILE__));
include $filepath."/./admin/config/config.php";
include $filepath."/./admin/libraries/Database.php";
$db = new Database();

if(isset($_POST['addCart'])){

    $productId      = $_POST['productId'];
    $quantity       = $_POST['qty'];

    if($productId) {

        $sql = "SELECT id,product_name,slug,feature_image,price,discount_price FROM product WHERE id=:productId";
        $db->query($sql);
        $db->bind(":productId", $productId);
        $db->execute();
        $product = $db->single();

        if (!empty($product)) {

            /* product price set*/
            if ($product->discount_price <= 0) {
                $price = $product->price;
            } else {
                $price = $product->discount_price;
            }
            if(isset($_SESSION["cart"]))
            {
                $is_available = 0;
                foreach($_SESSION["cart"] as $keys => $values)
                {
                    if($_SESSION["cart"][$keys]['product_id'] == $product->id)
                    {
                        $is_available++;
                        $_SESSION["cart"][$keys]['qty'] = $_SESSION["cart"][$keys]['qty'] + $quantity;
                    }
                }
                if($is_available == 0)
                {
                    $item_array = array(
                        'product_id'  => $product->id,
                        'qty'         => $quantity,
                        'price'       => $price,
                        'productName'  =>$product->product_name,
                        'slug'         =>$product->slug,
                        'image'         =>$product->feature_image,
                    );
                    $_SESSION["cart"][] = $item_array;
                }
                /*return success message after add product to cart*/
                $_SESSION['toastr'] = array(
                    'type'      => 'success', // or 'success' or 'info' or 'warning'
                    'message'   => 'Product Successfully added to your cart',
                    'title'     => 'Success'
                );
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }
            else
            {
                $item_array = array(
                    'product_id'  => $product->id,
                    'qty'         => $quantity,
                    'price'       => $price,
                    'productName'  =>$product->product_name,
                    'slug'        =>$product->slug,
                    'image'         =>$product->feature_image,
                );

                $_SESSION["cart"][] = $item_array;

                /*return success message after add product to cart*/
                $_SESSION['toastr'] = array(
                    'type'      => 'success', // or 'success' or 'info' or 'warning'
                    'message'   => 'Product Successfully added to your cart',
                    'title'     => 'Success'
                );
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }




        }

    }

}