-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 11, 2022 at 09:45 AM
-- Server version: 5.7.34
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eshop43_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `brand_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `brand_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `brand_name`, `slug`, `brand_logo`, `status`) VALUES
(1, 'HP', 'hp', 'uploads/brand/1647057333622c19b5c6441.png', 1),
(2, 'Apple', 'apple', 'uploads/brand/1647057344622c19c0e5e97.png', 1),
(3, 'Asus', 'asus', 'uploads/brand/1647057355622c19cb50f45.png', 1),
(4, 'Aarong', 'aarong', 'uploads/brand/1647706848623602e03f853.png', 1),
(5, 'oneplus', 'oneplus', 'uploads/brand/16477068956236030fd314c.png', 1),
(6, 'HATIL', 'hatil', 'uploads/brand/164770692862360330e49e3.jfif', 1),
(7, 'RFL', 'rfl', 'uploads/brand/16477069546236034ab1719.png', 1),
(8, 'SQUARE', 'square', 'uploads/brand/1647706981623603659f6f6.png', 1),
(9, 'PRAN', 'pran', 'uploads/brand/1647706996623603743bba3.png', 1),
(10, 'Unilever', 'unilever', 'uploads/brand/16477070186236038ab0073.png', 1),
(11, 'KOHINOOR CHEMICAL', 'kohinoor-chemical', 'uploads/brand/1647707069623603bda7cac.jfif', 1),
(12, 'WALTON', 'walton', 'uploads/brand/16477079336236071d06ec5.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `slug`, `status`) VALUES
(5, 'Electronics &amp; Gadgets', 'electronics-amp-gadgets', 1),
(6, 'Fashion &amp; Lifestyle', 'fashion-amp-lifestyle', 1),
(7, 'Home &amp; Living', 'home-amp-living', 1),
(8, 'Health &amp; Beauty', 'health-amp-beauty', 1),
(9, 'Daily Needs', 'daily-needs', 1),
(10, 'Sports &amp; Outdoor', 'sports-amp-outdoor', 1),
(11, 'Food', 'food', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `address` text,
  `shipping_add` text,
  `_token` varchar(255) NOT NULL,
  `is_verified` enum('Yes','No') NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `password`, `mobile`, `address`, `shipping_add`, `_token`, `is_verified`) VALUES
(8, 'HAFIZUR RAHMAN', 'hafizuritp20@gmail.com', '$2y$10$05UW9108KAYAEn4r9coD8O2PmLZzhsc34eaCK2FGOoNOvgCaTtdCe', '01739981172', NULL, 'Good luck center, panthapath ],dhaka', '6c1c6ff2823ee50cb6876721ae8a7f23', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `total_item` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `shaping_charge` int(11) NOT NULL,
  `status` enum('Pending','Confirm','Processing','Delivery','Cancel') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pending',
  `payment_type` enum('COD','Online') COLLATE utf8_unicode_ci DEFAULT NULL,
  `transaction_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_status` enum('Unpaid','Paid') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Unpaid'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `order_number`, `order_date`, `total_item`, `total_amount`, `shaping_charge`, `status`, `payment_type`, `transaction_id`, `currency`, `payment_status`) VALUES
(1, 8, '20220411202482', '2022-04-11 03:36:22', 2, 2471, 30, 'Pending', 'COD', NULL, NULL, 'Unpaid'),
(2, 8, '20220411545813', '2022-04-11 03:38:32', 1, 400, 30, 'Pending', 'COD', NULL, NULL, 'Unpaid');

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `sub_total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`id`, `order_id`, `product_name`, `price`, `quantity`, `sub_total`) VALUES
(1, 1, 'Cotton Short Sleeve Polo for Men', 713, 1, 713),
(2, 1, 'Alex Black Khimar with Skirt Set', 1758, 1, 1758),
(3, 2, 'AR Vitamin E &amp; C Body Cream 200ml', 400, 1, 400);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `product_name` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_description` text COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `discount` int(11) NOT NULL,
  `discount_price` decimal(10,0) NOT NULL,
  `quantity` int(11) NOT NULL,
  `feature_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `view_count` int(11) DEFAULT NULL,
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `category_id`, `sub_category_id`, `brand_id`, `product_name`, `slug`, `short_description`, `description`, `price`, `discount`, `discount_price`, `quantity`, `feature_image`, `view_count`, `is_featured`, `status`) VALUES
(3, 5, 8, 1, '2 Pcs- HP Pendrive 32 GB', '2 Pcs- HP Pendrive 32 GB', 'Brand: HpUltra-lightweight.High Transfer Speeds.Capacity: 32GBPack of contains: 2 PcsExtremely Slim and Portable', 'Full Metal.\r\n32 GB Capacity.\r\nUSB 3.1\r\nSuitable for Smart Devices.\r\nEasy Plug and Play installation', '980', 5, '931', 50, 'uploads/products/1647707644-623605fcc044a.jpg', NULL, 0, 1),
(4, 5, 7, 2, 'iPhone 12 Blue 64GB', 'iphone-12-blue-64gb', 'Brand: AppleProduct Type: iPhone 12Capacity: 64 GBColor: Blue', '6.1 â€“ inch Super Retina XDR display\r\nSplash, Water, and Dust Resistant (IP68)\r\nDual-camera System with 12MP Ultra Wide\r\nand Wide cameras; Night mode, Portrait mode\r\nand 4K video up to 60 fps\r\n12MP TrueDepth front camera with Portrait\r\nmode, 4K video, and slow-motion video\r\nFace ID for secure authentication and Apple Pay\r\nA 14 Bionic chip with Next-generation\r\nNeural Engine\r\nFinish â€“ Blue\r\nWarranty: 1 Year International Warranty ( Parts and Labor both).\r\n\r\nWarranty will not apply for the below reasons:\r\nIf the product damaged or broken by the customer.\r\nIf the product damaged by water or fire.', '115999', 10, '104399', 1, 'uploads/products/1649397796-624fd024bc11d.jpg', NULL, 1, 1),
(5, 9, 16, 12, 'Whirlpool Washing Machine Dry Linen 360UC- Graphite- 14KG', 'whirlpool-washing-machine-dry-linen-360uc-graphite-14kg', 'Brand: WhirlpoolProduct Type: Washing MachineModel: 360UC GraphiteWeight: 14KGColor: Graphite', 'Net Quantity: 1 N\r\nCapacity (kg): 14\r\n6TH SENSEÂ® Technology: Yes\r\nIn built heater: Yes\r\nExpress wash: Yes\r\nAuto tub clean: Yes\r\nSoftner dispenser: Yes\r\nCaremove: Yes\r\nCatalytic Soak: Yes\r\nDimensions ( WxDxH): 620*670*1090\r\nMaximum spin speed: 740 RPM  \r\nWattage: 770 W\r\nVoltage: 220V , 50Hz\r\nHot finish: Yes\r\nPower dry: Yes\r\nQuiet comfort: Yes\r\nHot catalytic shower: Yes\r\nDynamix technology: Yes\r\nChild lock: Yes\r\nMagic lint filter: Yes\r\nDrum: Steel - spa drum\r\nColor: Graphite\r\nWater levels: 10\r\nWash Programs: Daily, Heavy, Delicate, Whites, Stainwash, Baby Care, Woollens, Hot Finish, Sari, Rinse+Dry, Dry Only, Wash Only\r\n\r\n \r\n\r\nWarranty:\r\n\r\nService Warranty: 24 Month\r\nParts Warranty: 24 Month\r\nSpecial Component Warranty: 120 Month', '42900', 5, '40755', 1, 'uploads/products/1647708313-623608993555a.jpg', NULL, 1, 1),
(6, 6, 9, 4, 'Cotton Short Sleeve Polo for Men', 'cotton-short-sleeve-polo-for-men', 'Brand: Cotton KraftProduct Type: PoloMaterial: 100 % cottonSize: L, XLColor: Black', 'High Quality\r\nAttractive and fashionable design\r\nComfortable Regular Fit\r\nGSM- 210\r\nSize:\r\n\r\nL:  Chest-40 Length-28\r\nXL:  Chest-42 Length-29', '750', 5, '713', 1, 'uploads/products/1647708421-62360905a433f.jpg', NULL, 1, 1),
(7, 6, 10, 4, 'Alex Black Khimar with Skirt Set', 'alex-black-khimar-with-skirt-set', 'Product Type:  Khimar with SkirtSize: Free Gender: WomenColor: Black', 'Main Material:  Alex\r\nFabric:  Alex\r\nSize: Free \r\nThis product is an Islamic dress. Simple, loose over-garment, essentially a robe-like dress, worn by some women in parts of the Muslim people.', '1850', 5, '1758', 1, 'uploads/products/1647708545-623609812a2c1.jpg', NULL, 1, 1),
(8, 11, 15, 9, 'Combo of 24 Pack Fine Dine Peanut Butter', 'combo-of-24-pack-fine-dine-peanut-butter', 'Brand: Fine DineProduct Type: Peanut Butter.Pack Size: 8.16 Kg (340*24) gmOrigin: China', 'Peanut Butter China origin Fine Dine.\r\nThis product is very helpful for every person.', '7920', 15, '6732', 1, 'uploads/products/1647708678-62360a06cfc60.jpg', NULL, 0, 1),
(9, 5, 8, 3, 'GAMDIAS POSEIDON M2 4 In 1 Gaming COMBO', 'gamdias-poseidon-m2-4-in-1-gaming-combo', 'Brand: GamdiasProduct Type: Gaming KeyboardModel: Gamdias POSEIDON M2 4 in 1Keyboard, Mouse, Mouse Mat, And Headphone Gaming ComboGAMDIAS Certified Membrane SwitchesDPI: 1200/1800/ 2400/ 3600 DPIMicrophone Sensitivity: -58dbÂ±3db', 'Cable Length: 1.5 m\r\nDimension(LxWxH): 470 x 200 x 55 mm\r\nInterface: USB\r\nKey Switch: GAMDIAS Certified Membrane Switches\r\nMultimedia Keys: 8\r\nSwitch Lifecycle: 10 Million\r\nWeight: 0.65 kg\r\nMouse\r\n\r\nCable Length: 1.5m\r\nConnector: USB\r\nDimension(LxWxH): 120 x 75 x 40 mm\r\nOthers\r\nMouse Mat:\r\nTracking Method: Professional Gaming Mouse Mat\r\nDimension(L xW xH): 240 x 180 x 3mm\r\nResolution: 1200/1800/ 2400/ 3600 DPI (Default 1200)\r\nSwitch Lifecycle: 1 Million\r\nTracking Method: Advanced Gaming Optical Sensor\r\nWeight: 126g\r\nHeadphone:\r\nCable Length: 1.8m\r\nDiameter: 40mm\r\nDimension(LxWxH): 170 x 100 x 215 mm\r\nImpedance: 32 Ohm + / â€‹â€‹- 15%\r\nLighting Effect: Multi-color\r\nMicrophone Sensitivity: -58dbÂ±3db\r\nMicrophone Size: Î¦6*2.7mm\r\nSensitivity: 109+3dB\r\nWeight: 281g', '1633333', 5, '1551666', 1, 'uploads/products/1647709303-62360c77e8ac9.jpg', NULL, 1, 1),
(10, 8, 14, 10, 'AR Vitamin E &amp; C Body Cream 200ml', 'AR Vitamin E &amp; C Body Cream 200ml', 'Brand: RR Beauty TherapySize: 200 mlProduct Type: CreamCounty of Origin: Thailand', 'Vitamin E&amp;C\r\nMoisturizing\r\nWhitening\r\nBrightening &amp; Glow\r\nSilky Soft, Smooth, Radiant Skin', '500', 20, '400', 1, 'uploads/products/1647709574-62360d865afd9.jpg', NULL, 0, 1),
(11, 8, 14, 8, 'BIOAQUA VC Facial Water Vitamin C Essence - 500 ml', 'bioaqua-vc-facial-water-vitamin-c-essence-500-ml', 'Brand: BioaquaProduct Type: Facial WaterWeight: 500 mlCountry of Origin: China', 'Brand: BioaquaProduct Type: Facial WaterWeight: 500 mlCountry of Origin: China', '1500', 30, '1050', 1, 'uploads/products/1647709692-62360dfc0e8a9.jpg', NULL, 0, 1),
(12, 5, 17, 2, 'Apple MacBook Air 13.3-Inch 10th Gen Core i5-1.1GHz, 8GB RAM, 512GB SSD Space Gray 2020 (MVH22LL/A)', 'apple-macbook-air-13-3-inch-10th-gen-core-i5-1-1ghz-8gb-ram-512gb-ssd-space-gray-2020-mvh22ll-a-', 'Brand: AppleProduct Type:  MacBook Memory: 8GBStorage: 512 SSDSize: 13.3-InchColor:  Space Gray', 'Retina display:\r\n\r\n13.3-inch (diagonal) LED-backlit display with IPS technology; 2560-by-1600 native resolution at 227 pixels per inch with support for millions of colors\r\nSupported scaled resolutions:\r\n1680 by 1050\r\n1440 by 900\r\n1024 by 640\r\n400 nits brightness\r\nWide color (P3)\r\nTrue Tone technology\r\nChip:\r\n\r\nApple M1 chip\r\n8-core CPU with 4 performÂ­ance cores and 4 efficiency cores\r\n8-core GPU\r\n16-core Neural Engine\r\n\r\nBattery and Power:\r\n\r\nUp to 15 hours wireless web.\r\nUp to 18 hours of Apple TV app movie playback.\r\nBuilt-in 49.9â€‘watt-hour lithiumâ€‘polymer battery.\r\n30W USB-C Power Adapter.\r\nChargÂ­ing and ExpanÂ­sion:\r\n\r\nTwo Thunderbolt / USB 4 ports with support for:\r\nCharging\r\nDisplayPort\r\nThunderbolt 3 (up to 40Gb/s)\r\nUSB 4 (up to 40Gb/s)\r\nUSB 3.1 Gen 2 (up to 10Gb/s)\r\n\r\nMemory:\r\n\r\n8GB unified memory\r\nConfigurable to: 16GB\r\n\r\nStorage:\r\n\r\n512GB SSD\r\nConfigurable to: 1TB or 2TB\r\n\r\nKeyÂ­board and TrackÂ­pad\r\nBacklit Magic Keyboard with:\r\n\r\n78 (U.S.) or 79 (ISO) keys including 12 function keys and 4 arrow keys in an inverted-T arrangement.\r\nAmbient light sensor.\r\nForce Touch trackpad for precise cursor control and pressure-sensing capabilities; enables Force clicks, accelerators, pressure-sensitive drawing, and Multi-Touch gestures.\r\nTouch ID: \r\nTouch ID sensor\r\n\r\n\r\nWireless:\r\n\r\nWi-Fi:\r\n802.11ax Wi-Fi 6 wireless networking\r\nIEEE 802.11a/b/g/n/ac compatible\r\n\r\nBluetooth:\r\n\r\nBluetooth 5.0 wireless technology\r\nCamera:720p FaceTime HD camera\r\n\r\nVideo Support:\r\n\r\nSimultaneously supports full native resolution on the built-in display at millions of colors and One external display with up to 6K resolution at 60Hz.\r\nThunderbolt 3 digital video output.\r\nNative DisplayPort output over USB-C\r\nVGA, HDMI, DVI, and Thunderbolt 2 output supported using adapters (sold separately).\r\n \r\n\r\nAudio:\r\n\r\nStereo speakers\r\nWide stereo sound\r\nSupport for Dolby Atmos playback\r\nThree-mic array with directional beamforming\r\n3.5 mm headphone jack\r\n\r\nSize and Weight:\r\n\r\nHeight: 0.16â€“0.63 inch (0.41â€“1.61 cm)\r\nWidth: 11.97 inches (30.41 cm)\r\nDepth: 8.36 inches (21.24 cm)\r\nWeight: 2.8 pounds (1.29 kg)3\r\n\r\nWarranty:\r\n\r\nOfficial 1-year warranty\r\nService warranty 1 Year. \r\n\r\nWarranty not applicable if: \r\n1. Physical damage \r\n2.Broken issue\r\n3.Burn issue/water issue\r\n4.Tab on the body', '141000', 10, '126900', 1, 'uploads/products/1647709970-62360f12d03eb.jpg', NULL, 1, 1),
(13, 7, 12, 6, '(5 in 1) Bed come sofa with free pumper', '-5-in-1-bed-come-sofa-with-free-pumper', 'A relaxed recliner; A children\'s high rise sleeper, a blissful queen size bedSize: LxBxH - 74x60x25 Inch (Thickness : 7.5 Inch)', 'A relaxed recliner; A children\'s high rise sleeper, a blissful queen size bedSize: LxBxH - 74x60x25 Inch (Thickness : 7.5 Inch)', '5500', 5, '5225', 1, 'uploads/products/1647710192-62360ff02d16b.jpg', NULL, 0, 1),
(14, 10, 18, 12, 'Begasso 10 knives Mountain Folding Bicycle 26&quot; - (Chili)', 'Begasso 10 knives Mountain Folding Bicycle 26&quot; - (Chili)', 'Brand: BegassoProduct Type: Folding BicycleFrame Material: High Carbon SteelSwitching Gear Control systemDouble SuspensionColor: Chili', 'Cycle fitting is 60 -70 % done in the carton. The customer needs to set-up the rest in their own way                                           \r\nEasy to carry\r\nYou can take it in any place.\r\nGear System: Shimano (7 + 3) Gear.\r\nMinimum Speed: 21 km/hour\r\nSwitching Gear Control system\r\nComfortable Double Suspension \r\nThere is no problem for keeping in garage\r\nAttractive color and stylish looking\r\nAvailable parts can be found in Bangladesh\r\nNet Weight: 17 kg\r\nGross Weight: 20 kg\r\nHeight: 26 inches\r\nWeight capacity: 140-160 kg\r\nDouble disc and hydraulic brake\r\nFrame Material: High Carbon Steel\r\nFront suspension: Still   \r\nRim Material: Aluminum \r\nPaddle: Comfortable\r\nTire &amp; Tube: Air Type (Tube)\r\nWheel type: 03 knives', '20000', 15, '17000', 1, 'uploads/products/1647710501-62361125d639c.jpg', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `product_image`) VALUES
(3, 3, '60697af3c4530_1617525491.jpg'),
(4, 4, '60a62b15e0c9e_1621502741.png'),
(5, 5, '610579086f43b_1627748616.jpg'),
(6, 6, '61337bd390e72_1630763987.jpg'),
(7, 7, '60405cb2e9506_1614830770.jpg'),
(8, 8, '604752bbbad7d_1615286971.jpg'),
(9, 9, '613e00484e296_1631453256.jpg'),
(10, 10, '601bde98daf52_1612439192.jpg'),
(11, 11, '6139f4c87d5ce_1631188168.jpg'),
(12, 12, '60bcbf27132c2_1622982439.jpg'),
(13, 13, '60572eb870dc0_1616326328.jpg'),
(14, 14, '60cef230c47e3_1624175152.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` double NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `product_id`, `title`, `sub_title`, `price`, `image`, `status`) VALUES
(3, 1, 'Intel 11th Gen Core i7-11700 Special Gaming PC', '5 OFF', 170000, 'uploads/slider/1647580749-6234164d09b97.png', 1),
(4, 2, 'HP 15s-du1088TU Intel Pentium N5030 15.6 inch FHD Laptop with Win 10', '05 OFF', 90000, 'uploads/slider/1647578754-62340e82070c6.png', 1),
(5, 1, 'Intel 11th Gen Core i7-11700 Special Gaming PC', 'New', 170000, 'uploads/slider/1647705335-6235fcf72f8c9.png', 1),
(6, 14, 'Begasso 10 knives Mountain Folding Bicycle 26&amp;quot; - (Chili)', 'Hot', 20000, 'uploads/slider/1647710793-6236124909d1c.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `category_id`, `name`, `slug`, `status`) VALUES
(7, 5, 'Mobiles &amp; Tablets', 'mobiles-amp-tablets', 1),
(8, 5, 'Computer Accessories', 'computer-accessories', 1),
(9, 6, 'Men', 'men', 1),
(10, 6, 'Women', 'women', 1),
(11, 8, 'Make Up', 'make-up', 1),
(12, 7, 'Furniture &amp; DÃ©cor', 'furniture-amp-dcor', 1),
(13, 9, 'Water &amp; Soft Drinksv', 'water-amp-soft-drinksv', 1),
(14, 8, 'Hair &amp; Skin Care', 'hair-amp-skin-care', 1),
(15, 11, 'Dairy Products', 'dairy-products', 1),
(16, 9, 'Machine', 'machine', 1),
(17, 5, 'Computers &amp; Laptops', 'computers-amp-laptops', 1),
(18, 10, 'Cycling', 'cycling', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('admin','manager','executive') COLLATE utf8_unicode_ci NOT NULL,
  `verfication_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_verified` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `udpate_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `name`, `email`, `password`, `type`, `verfication_code`, `is_verified`, `created_at`, `udpate_at`) VALUES
(1, 'Mr. Admin', 'admin@mail.com', '$2y$10$sEFVLhv0qPMLaF/c5tVwQOykRAnuWRyiz3oK0yH9Ck4oQ4Bpz1tBm', 'admin', NULL, 1, '2022-02-12 05:18:20', '2022-02-12 05:18:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categoryIndex` (`name`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customerIdIndex` (`customer_id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderIdIndex` (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categorySubCategoryBrandIndex` (`category_id`,`sub_category_id`,`brand_id`),
  ADD KEY `brand_id` (`brand_id`),
  ADD KEY `sub_category_id` (`sub_category_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productIdIndex` (`product_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productIdIndex` (`product_id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoryInex` (`category_id`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD CONSTRAINT `sub_category_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
