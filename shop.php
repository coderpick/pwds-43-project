<?php
include "layout/head.php";
?>

<body>

<!-- Page Wrapper -->
<div id="wrap" class="layout-1">

    <!-- Top bar -->

    <?php
    include "layout/top_bar.php";
    ?>
    <!-- Header -->
    <?php
    include "layout/header.php";
    ?>


    <!-- Content -->
    <div id="content">

        <!-- Products -->
        <section class="padding-top-40 padding-bottom-60m  mt-4">
            <div class="container">
                <div class="row">

                    <!-- Shop Side Bar -->
                    <div class="col-md-3">
                        <?php
                        include "shop_page_sidebar.php"
                        ?>
                    </div>

                    <!-- Products -->
                    <div class="col-md-9">

                        <!-- Short List -->
                        <div class="short-lst">
                            <h2>All Products</h2>
                        </div>

                        <!-- Items -->
                        <div class="item-col-3">

                            <?php

                            if (isset($_GET['category']) && isset($_GET['subcategory'])) {

                                $categoryId    = $_GET['category'];
                                $subCategoryId = $_GET['subcategory'];
                                $products      = $product->categoryAndSubCategoryProduct($categoryId, $subCategoryId);
                            } elseif (isset($_GET['category'])) {

                                $categoryId = $_GET['category'];

                                $products = $product->categoryProduct($categoryId);

                            } elseif (isset($_GET['brand'])) {

                                $brandSlug  = $_GET['brand'];

                                $products = $product->brandProduct($brandSlug);

                            } else {

                                $products = $product->getAllProducts();
                            }


                            if ($products != null) {
                                foreach ($products as $product) { ?>
                                    <!-- Product -->
                                    <div class="product">
                                        <article> <img class="img-responsive" src="admin/<?php echo $product->feature_image; ?>" alt="">
                                            <?php
                                            if (isset($product->discount)) {
                                                echo '  <span class="sale-tag">-' . $product->discount . '%</span>';
                                            }
                                            ?>


                                            <!-- Content -->
                                            <span class="tag"><?php echo $product->categoryName; ?></span>
                                            <a href="product.php?slug=<?php echo $product->slug; ?>" class="tittle">
                                                <?php echo $product->product_name; ?>
                                            </a>

                                            <?php
                                            if (isset($product->discount)) {
                                                echo '<div class="price"> 
                                                        ' . $product->discount_price . '                                           
                                                       <span>' . $product->price . '</span></div>';
                                            } else {
                                                echo '<div class="price">' . $product->price . '</div>';
                                            }
                                            ?>
                                            <form action="addToCart.php" method="post">
                                                <input type="hidden" name="qty"  value="1">
                                                <input type="hidden" name="productId" value="<?php echo $product->id; ?>">
                                                <button type="submit" style="border: none;" name="addCart" class="cart-btn"><i class="icon-basket-loaded"></i>
                                                </button>
                                            </form>
                                        </article>
                                    </div>
                                <?php       }
                            }
                            ?>



                            <!-- pagination -->
                            <ul class="pagination">
                                <li> <a href="GridProducts_4Columns.html#" aria-label="Previous"> <i class="fa fa-angle-left"></i> </a> </li>
                                <li><a class="active" href="GridProducts_4Columns.html#">1</a></li>
                                <li><a href="GridProducts_4Columns.html#">2</a></li>
                                <li><a href="GridProducts_4Columns.html#">3</a></li>
                                <li> <a href="GridProducts_4Columns.html#" aria-label="Next"> <i class="fa fa-angle-right"></i> </a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Your Recently Viewed Items -->
        <section class="padding-bottom-60">
            <div class="container">

                <!-- heading -->
                <div class="heading">
                    <h2>Your Recently Viewed Items</h2>
                    <hr>
                </div>
                <!-- Items Slider -->
                <div class="item-slide-5 with-nav owl-carousel owl-theme owl-loaded">
                    <!-- Product -->


                    <div class="owl-stage-outer">
                        <div class="owl-stage" style="transform: translate3d(-1638px, 0px, 0px); transition: all 0.25s ease 0s; width: 4212px;">
                            <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-4.jpg" alt=""> <span class="new-tag">New</span>

                                        <!-- Content -->
                                        <span class="tag">Accessories</span> <a href="GridProducts_4Columns.html#." class="tittle">Teclado Inalambrico Bluetooth Con Air Mouse</a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00</div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-5.jpg" alt="">
                                        <!-- Content -->
                                        <span class="tag">Appliances</span> <a href="GridProducts_4Columns.html#." class="tittle">Funda Para Ebook 7" 128GB full HD</a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00</div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-6.jpg" alt=""> <span class="sale-tag">-25%</span>

                                        <!-- Content -->
                                        <span class="tag">Tablets</span> <a href="GridProducts_4Columns.html#." class="tittle">Mp3 Sumergible Deportivo Slim Con 8GB</a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00 <span>$200.00</span></div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-7.jpg" alt="">
                                        <!-- Content -->
                                        <span class="tag">Appliances</span> <a href="GridProducts_4Columns.html#." class="tittle">Reloj Inteligente Smart Watch M26 Touch Bluetooh </a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00</div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-8.jpg" alt=""> <span class="new-tag">New</span>

                                        <!-- Content -->
                                        <span class="tag">Accessories</span> <a href="GridProducts_4Columns.html#." class="tittle">Teclado Inalambrico Bluetooth Con Air Mouse</a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00</div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-1.jpg" alt="">
                                        <!-- Content -->
                                        <span class="tag">Latop</span> <a href="GridProducts_4Columns.html#." class="tittle">Laptop Alienware 15 i7 Perfect For Playing Game</a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00 </div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-2.jpg" alt=""> <span class="sale-tag">-25%</span>

                                        <!-- Content -->
                                        <span class="tag">Tablets</span> <a href="GridProducts_4Columns.html#." class="tittle">Mp3 Sumergible Deportivo Slim Con 8GB</a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00 <span>$200.00</span></div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item active" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-3.jpg" alt="">
                                        <!-- Content -->
                                        <span class="tag">Appliances</span> <a href="GridProducts_4Columns.html#." class="tittle">Reloj Inteligente Smart Watch M26 Touch Bluetooh </a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00</div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item active" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-4.jpg" alt=""> <span class="new-tag">New</span>

                                        <!-- Content -->
                                        <span class="tag">Accessories</span> <a href="GridProducts_4Columns.html#." class="tittle">Teclado Inalambrico Bluetooth Con Air Mouse</a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00</div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item active" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-5.jpg" alt="">
                                        <!-- Content -->
                                        <span class="tag">Appliances</span> <a href="GridProducts_4Columns.html#." class="tittle">Funda Para Ebook 7" 128GB full HD</a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00</div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item active" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-6.jpg" alt=""> <span class="sale-tag">-25%</span>

                                        <!-- Content -->
                                        <span class="tag">Tablets</span> <a href="GridProducts_4Columns.html#." class="tittle">Mp3 Sumergible Deportivo Slim Con 8GB</a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00 <span>$200.00</span></div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item active" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-7.jpg" alt="">
                                        <!-- Content -->
                                        <span class="tag">Appliances</span> <a href="GridProducts_4Columns.html#." class="tittle">Reloj Inteligente Smart Watch M26 Touch Bluetooh </a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00</div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-8.jpg" alt=""> <span class="new-tag">New</span>

                                        <!-- Content -->
                                        <span class="tag">Accessories</span> <a href="GridProducts_4Columns.html#." class="tittle">Teclado Inalambrico Bluetooth Con Air Mouse</a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00</div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-1.jpg" alt="">
                                        <!-- Content -->
                                        <span class="tag">Latop</span> <a href="GridProducts_4Columns.html#." class="tittle">Laptop Alienware 15 i7 Perfect For Playing Game</a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00 </div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-2.jpg" alt=""> <span class="sale-tag">-25%</span>

                                        <!-- Content -->
                                        <span class="tag">Tablets</span> <a href="GridProducts_4Columns.html#." class="tittle">Mp3 Sumergible Deportivo Slim Con 8GB</a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00 <span>$200.00</span></div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-3.jpg" alt="">
                                        <!-- Content -->
                                        <span class="tag">Appliances</span> <a href="GridProducts_4Columns.html#." class="tittle">Reloj Inteligente Smart Watch M26 Touch Bluetooh </a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00</div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-4.jpg" alt=""> <span class="new-tag">New</span>

                                        <!-- Content -->
                                        <span class="tag">Accessories</span> <a href="GridProducts_4Columns.html#." class="tittle">Teclado Inalambrico Bluetooth Con Air Mouse</a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00</div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                            <div class="owl-item cloned" style="width: 204px; margin-right: 30px;">
                                <div class="product">
                                    <article> <img class="img-responsive" src="images/item-img-1-5.jpg" alt="">
                                        <!-- Content -->
                                        <span class="tag">Appliances</span> <a href="GridProducts_4Columns.html#." class="tittle">Funda Para Ebook 7" 128GB full HD</a>
                                        <!-- Reviews -->
                                        <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                        <div class="price">$350.00</div>
                                        <a href="GridProducts_4Columns.html#." class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="owl-controls">
                        <div class="owl-nav">
                            <div class="owl-prev" style=""><i class="fa fa-angle-left"></i></div>
                            <div class="owl-next" style=""><i class="fa fa-angle-right"></i></div>
                        </div>
                        <div class="owl-dots" style="">
                            <div class="owl-dot active"><span></span></div>
                            <div class="owl-dot"><span></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Clients img -->
        <section class="light-gry-bg clients-img">
            <div class="container">
                <ul>
                    <li><img src="images/c-img-1.png" alt=""></li>
                    <li><img src="images/c-img-2.png" alt=""></li>
                    <li><img src="images/c-img-3.png" alt=""></li>
                    <li><img src="images/c-img-4.png" alt=""></li>
                    <li><img src="images/c-img-5.png" alt=""></li>
                </ul>
            </div>
        </section>

        <!-- Newslatter -->
        <section class="newslatter">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Subscribe our Newsletter <span>Get <strong>25% Off</strong> first purchase!</span></h3>
                    </div>
                    <div class="col-md-6">
                        <form>
                            <input type="email" placeholder="Your email address here...">
                            <button type="submit">Subscribe!</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <!-- End Content -->


    <!-- Footer -->
    <?php include "layout/footer.php"; ?>
    <!-- End Footer -->

    <!-- GO TO TOP  -->
    <a href="index.html#" class="cd-top"><i class="fa fa-angle-up"></i></a>
    <!-- GO TO TOP End -->
</div>
<!-- End Page Wrapper -->

<!-- JavaScripts -->
<?php include "layout/_script.php"; ?>
<!-- page related jquery plugin load here...-->


<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<?php include "layout/_script_activate.php"; ?>

<!-- custom js -->

</body>

</html>