<?php include_once "layout/head.php"; ?>

<body data-sidebar="dark">

<!-- Begin page -->
<div id="layout-wrapper">

    <?php include_once "layout/header.php"; ?>

    <!-- ========= = Left Sidebar Start ========= = -->
    <?php include_once "layout/sidebar.php"; ?>
    <!-- Left Sidebar End -->

    <!-- ============================================================= = -->
    <!-- Start right Content here -->
    <!-- ============================================================= = -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Product Create</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">product</a></li>
                                    <li class="breadcrumb-item active">create</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!--//.row end-->
                <div class="row">
                    <div class="col-12">
                        <div class="card rounded shadow-lg">
                            <div class="card-body">
                                <div class="text-right mb-2">
                                    <a href="productIndex.php" class="btn btn-pink"><i class="fa fa-reply"></i> Back to list</a>
                                </div>
                                <hr>
                                <?php
                                if (isset($_POST['submit'])) {

                                    $data = array(
                                        'category'          => $helper->validate($_POST['category']),
                                        'sub_category'      => $helper->validate($_POST['sub_category']),
                                        'brand'             => $helper->validate($_POST['brand']),
                                        'product_name'      => $helper->validate($_POST['product_name']),
                                        'product_slug'      => $helper->validate($_POST['product_slug']),
                                        'quantity'          => $helper->validate($_POST['quantity']),
                                        'price'             => $helper->validate($_POST['price']),
                                        'discount'          => $helper->validate($_POST['discount']),
                                        'discount_price'    => $helper->validate($_POST['discount_price']),
                                        'short_description' => $helper->validate($_POST['short_description']),
                                        'description'       => $helper->validate($_POST['description']),
                                        'is_featured'       => $helper->validate($_POST['is_featured']),
                                        'status'            => $helper->validate($_POST['status']),
                                        'productId'         => $_POST['productId'],
                                    );
                                    $oldFeatureImage        = $_POST['oldFeatureImage'];

                                    if (empty($data['category'])) {
                                        $error['category'] = "Category is required";
                                    }
                                    if (empty($data['brand'])) {
                                        $error['brand'] = "Brand is required";
                                    }
                                    if (empty($data['product_name'])) {
                                        $error['product_name'] = "Product name is required";
                                    }
                                    if (empty($data['product_slug'])) {
                                        $error['product_slug'] = "Product slug is required";
                                    }
                                    if (empty($data['quantity'])) {
                                        $error['quantity'] = "Quantity is required";
                                    }
                                    if (empty($data['price'])) {
                                        $error['price'] = "Price is required";
                                    }
                                    if (empty($data['description'])) {
                                        $error['description'] = "Description is required";
                                    }

                                    $featureImageName     = $_FILES['feature_image']['name'];
                                    $featureImageTmpName  = $_FILES['feature_image']['tmp_name'];
                                    $featureImageSize     = $_FILES['feature_image']['size'];
                                    $ext        = strtolower(pathinfo($featureImageName, PATHINFO_EXTENSION));
                                    $newImage   = time().'-'.uniqid().'.'.$ext;
                                    $uploadDir  = "uploads/products/";
                                    $uploadFeatureImage = $uploadDir.$newImage;
                                    $allExtension = array('jpeg','jpg','png');
                                    move_uploaded_file($featureImageTmpName,$uploadFeatureImage);
                                    if ($featureImageName ){

                                        if (in_array($ext,$allExtension)){
                                            if ($featureImageSize > 1048576){
                                                  $error['feature_image'] = "Feature image must be less then 1mb";
                                            }else{
                                                unlink($oldFeatureImage);
                                                 move_uploaded_file($featureImageTmpName,$uploadFeatureImage);
                                                 $data['feature_image'] = $uploadFeatureImage;
                                            }
                                        }else{
                                             $error['feature_image'] = "Feature image only jpg jpeg & png allow";
                                        }

                                    }
                                    else{
                                          $data['feature_image']  = $oldFeatureImage;
                                    }

                                    if (
                                        empty($error['category']) &&
                                        empty($error['brand']) &&
                                        empty($error['product_name']) &&
                                        empty($error['product_slug']) &&
                                        empty($error['quantity']) &&
                                        empty($error['price']) &&
                                        empty($error['description']) &&
                                        empty($error['feature_image'])
                                    ) {

                                        $insertProduct = $product->update($data);

                                        if (isset($insertProduct)) {
                                            echo  $insertProduct;
                                           echo '<meta http-equiv="refresh" content="1;url=productIndex.php" />';
                                        }

                                    }
                                }
                                /*get product id for edit data*/
                                if (isset($_GET['id'])){
                                    $id = trim($_GET['id']);
                                    $product = $product->show($id);

                                }
                                ?>
                                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="category">Select Category</label>
                                                <select class="form-control" name="category" id="category" >
                                                    <option value="">Select Category</option>
                                                    <?php
                                                    $categories = $category->index();
                                                    if ($categories != null) {
                                                        foreach ($categories as $category) { ?>
                                                            <option <?php echo isset($product->category_id )? ($product->category_id == $category->id)?'selected':'':''; ?> value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
                                                        <?php      }
                                                    }


                                                    ?>
                                                </select>
                                                <div class="text-danger">
                                                    <?php echo $error['category'] ?? ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="sub_category">Select Sub Category</label>
                                                <select name="sub_category" class="form-control" id="sub_category">
                                                    <?php
                                                    $subCategories = $subCategory->index();
                                                    if ($subCategories != null) {
                                                        foreach ($subCategories as $subCategory) { ?>
                                                            <option <?php echo isset($product->sub_category_id )? ($product->sub_category_id == $subCategory->id)?'selected':'':''; ?> value="<?php echo $subCategory->id; ?>"><?php echo $subCategory->name; ?></option>
                                                        <?php      }
                                                    }
                                                    ?>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="brand">Select Brand <span class="text-danger">*</span></label>
                                                <select name="brand" class="form-control" id="brand" >
                                                    <option value="">Select Brand</option>
                                                    <?php
                                                    $brands = $brand->index();
                                                    if ($brands != null) {
                                                        foreach ($brands as $brand) { ?>
                                                            <option <?php echo isset($product->brand_id )? ($product->brand_id == $brand->id)?'selected':'':''; ?> value="<?php echo $brand->id; ?>"><?php echo $brand->brand_name; ?></option>
                                                        <?php      }
                                                    }
                                                    ?>
                                                </select>
                                                <div class="text-danger">
                                                    <?php echo $error['brand'] ?? ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- //..row end -->

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="product_name">Product Name <span class="text-danger">*</span></label>
                                                <input type="text" name="product_name" value="<?php echo $product->product_name??''?>" class="form-control" id="product_name" >
                                                <div class="text-danger">
                                                    <?php echo $error['product_name'] ?? ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="product_slug">Product Slug <span class="text-danger">*</span></label>
                                                <input type="text" name="product_slug" value="<?php echo $product->slug??''?>" class="form-control" id="product_slug" >
                                                <div class="text-danger">
                                                    <?php echo $error['product_slug'] ?? ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--//.row end-->
                                    <div class="row">
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <label for="price">Price <span class="text-danger">*</span></label>
                                                <input type="text" name="price" value="<?php echo $product->price??''?>" class="form-control number-decimal" id="price" >
                                                <div class="text-danger">
                                                    <?php echo $error['price'] ?? ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <label for="discount">Discount(%)</label>
                                                <input type="text" name="discount" value="<?php echo $product->discount??''?>" class="form-control number" id="discount">
                                                <div class="text-danger">
                                                    <?php echo $error['discount'] ?? ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <label for="discount_price">Discount Price</label>
                                                <input type="number" name="discount_price" value="<?php echo $product->discount_price??''?>" class="form-control  number-decimal" id="discount_price" readonly>
                                                <div class="text-danger">
                                                    <?php echo $error['discount_price'] ?? ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <div class="form-group">
                                                <label for="quantity">Quantity <span class="text-danger">*</span></label>
                                                <input type="text" name="quantity" value="<?php echo $product->quantity??''?>" class="form-control number" id="quantity" >
                                                <div class="text-danger">
                                                    <?php echo $error['quantity'] ?? ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--//.row end-->

                                    <div class="row">
                                        <div class="col-md-6 col-lg-8">
                                            <div class="form-group">
                                                <label for="short_description">Short Description </label>
                                                <textarea name="short_description" rows="5" id="short_description" class="form-control"><?php echo $product->short_description??''?></textarea>
                                                <div class="text-danger">
                                                    <?php echo $error['short_description'] ?? ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-4">
                                            <div class="form-group">
                                                <label for="feature_image">Feature Image <span class="text-danger">*</span></label>
                                                <input type="file" name="feature_image"  data-default-file="<?php echo $product->feature_image??''?>" class="form-control dropify" data-max-file-size="1M" data-allowed-file-extensions="jpg jpeg png" id="feature_image" >
                                                <div class="text-danger">
                                                    <?php echo $error['feature_image'] ?? ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--//.row end-->
                                    <div class="form-group">
                                        <label for="description"> Description <span class="text-danger">*</span></label>
                                        <textarea name="description" rows="8" id="description" class="form-control summernote" ><?php echo $product->description??''?></textarea>
                                        <div class="text-danger">
                                            <?php echo $error['description'] ?? ''; ?>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="d-block font-weight-bold">Featured Product: <span class="text-danger">*</span></label>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo isset($product->is_featured)? ($product->is_featured ==true)?'checked':'':'';?> name="is_featured" id="inlineRadio1" value="1">
                                                    <label class="form-check-label" for="inlineRadio1">Yes</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo isset($product->is_featured)?($product->is_featured ==false)?'checked':'':'';?>  name="is_featured"  id="inlineRadio2" value="0">
                                                    <label class="form-check-label" for="inlineRadio2">No</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="d-block font-weight-bold">Status:<span class="text-danger">*</span> </label>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo isset($product->status)?($product->status ==true)?'checked':'':'';?> name="status" id="inlineRadio3" value="1">
                                                    <label class="form-check-label" for="inlineRadio3">Published</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" <?php echo isset($product->status)?($product->status ==false)?'checked':'':'';?> name="status"  id="inlineRadio4" value="0">
                                                    <label class="form-check-label" for="inlineRadio4">Drafted</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" name="productId" value="<?php echo $product->id??''; ?>">
                                    <input type="hidden" name="oldFeatureImage" value="<?php echo $product->feature_image??''; ?>">
                                    <div class="text-center">

                                        <div class="form-group">
                                            <button type="submit" name="submit" class="btn btn-primary btn-lg">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->



        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> © Skote.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-right d-none d-sm-block">
                            Design & Develop by pwds-43
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->
<!-- JAVASCRIPT -->
<?php include_once "layout/_script.php"; ?>
   <!-- Summernote js -->
   <script src="assets/libs/summernote/summernote-bs4.min.js"></script>

<script src="assets/dropify/js/dropify.min.js"></script>


<script>
    $(document).ready(function() {

        $(".dropify").dropify()
            $("#short_description").summernote({
                height:100
            })
            $("#description").summernote({
                height:250
            }) 

            

        $('#category').on('change', function() {

            let categoryId = $(this).val();

            $.ajax({
                type: "POST",
                url: "fetch_sub_cateogy.php",
                data: {
                    id: categoryId
                },
                success: function(response) {
                    //  console.log(response)
                    if (response) {
                        $('#sub_category').html(response);
                    }
                },
                error: function(error) {
                    console.log(error)
                }
            })

        })

        /* discount calucaltion */

        $('#discount').keyup(function() {
            let discount = $(this).val();
            let getPrice = $('#price').val();
            if (getPrice) {
                let discountPrice = getPrice - getPrice * discount / 100;
                $('#discount_price').val(discountPrice);
            } else {
                alert('Please enter product price first');
                $('#discount_price').val(' ');
            }
        })

        /* product slug generate */

        $("#product_slug").keyup(function() {
            var Text = $(this).val();
            Text = Text.toLowerCase();
            Text = Text.replace(/[^a-zA-Z0-9]+/g, '-');
            $("#product_slug").val(Text);
        });

        /* price field number and decimal only */
        $(".number-decimal").keypress(function(e) {
            if (e.which == 46) {
                if ($(this).val().indexOf('.') != -1) {
                    return false;
                }
            }

            if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
        /* only number */
        $(".number").keypress(function(event) {
            var key = event.which;

            if (!(key >= 48 && key <= 57))
                event.preventDefault();
        });
    });
</script>
</body>

</html>