<?php include_once "layout/head.php"; ?>

<body data-sidebar="dark">

    <!-- Begin page -->
    <div id="layout-wrapper">

        <?php include_once "layout/header.php"; ?>

        <!-- ========== Left Sidebar Start ========== -->
        <?php include_once "layout/sidebar.php"; ?>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="mb-0 font-size-18">Products List</h4>
                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Product</a></li>
                                        <li class="breadcrumb-item active">index</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!--//.row end-->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="text-right mb-2">
                                        <a href="addProduct.php" class="btn btn-success"><i class="fa fa-plus"></i> Add Product</a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <?php
                                    if (isset($_GET['delete']) && !empty($_GET['delete'])) {
                                        $id  = $_GET['delete'];
                                        $row = $product->delete($id);
                                    }
                                    ?>
                                    <table id="datatable" class="table table-bordered table-responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Sub Category</th>
                                                <th>Brand</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th>Featured</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $products = $product->index();
                                            if ($products) {
                                                $i = 1;
                                                foreach ($products as $product) { ?>
                                                    <tr>
                                                        <td><?php echo $i++; ?></td>
                                                        <td><?php echo $product->product_name ?></td>
                                                        <td>
                                                            <span class="badge badge-light"><?php echo $product->categoryName ?></span>
                                                        </td>
                                                        <td> <span class="badge badge-light"><?php echo $product->subCategoryName ?></span></td>
                                                        <td> <span class="badge badge-light"><?php echo $product->brand_name ?></span></td>
                                                        <td><?php echo $product->price ?></td>
                                                        <td><?php echo $product->quantity ?></td>
                                                        <td>
                                                            <?php
                                                            if ($product->is_featured == true) {
                                                                echo ' <span class="badge badge-success">Yes</span>';
                                                            } else {
                                                                echo ' <span class="badge badge-info">Yes</span>';
                                                            }
                                                            ?>

                                                        </td>
                                                        <td>
                                                            <?php if ($product->status == true) {
                                                                echo ' <span class="badge badge-primary">Active</span>';
                                                            } else {
                                                                echo ' <span class="badge badge-danger">Inactive</span>';
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <div class="dropdown">
                                                                <button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
                                                                    <i class="fa fa-plus-circle"></i>
                                                                </button>
                                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                    <li><a class="dropdown-item" href="showProduct.php?id=<?php echo $product->id ?>">Show Product</a></li>
                                                                    <li><a class="dropdown-item " href="editProduct.php?id=<?php echo $product->id ?>"">Edit Product</a></li>
                                                            <li><a class=" dropdown-item" onclick="deleteProduct(<?php echo $product->id ?>)">Delete Product</a></li>
                                                                    <li><a class="dropdown-item" href="productImages.php?id=<?php echo $product->id ?>">Product Images</a></li>
                                                                </div>
                                                            </div>
                                                            <form id="trash-form-<?php echo $product->id; ?>" action="deleteProduct.php" method="post" style="display: none;">
                                                                <input type="hidden" name="productId" value="<?php echo $product->id; ?>">
                                                            </form>
                                                        </td>
                                                    </tr>

                                            <?php     }
                                            }
                                            ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->

                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->


            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <script>
                                document.write(new Date().getFullYear())
                            </script> © Skote.
                        </div>
                        <div class="col-sm-6">
                            <div class="text-sm-right d-none d-sm-block">
                                Design & Develop by coderpick
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->


    <!-- JAVASCRIPT -->
    <?php include_once "layout/_script.php"; ?>
    <?php include_once "layout/datatable.php"; ?>
    <!-- ***********pages js file link below*******-->
 

    <script>
        function deleteProduct(id) {

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    if (result.value) {
                        document.getElementById('trash-form-' + id).submit();
                    }
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                }
            })
        }
    </script>
    <!-- JAVASCRIPT -->

    <?php include_once "layout/script_activate_file.php"; ?>
</body>

</html>