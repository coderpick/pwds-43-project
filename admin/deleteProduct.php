<?php
$filepath = realpath(dirname(__FILE__));
include_once $filepath . "/config/config.php";
include_once $filepath . "/libraries/Database.php";
$db = new Database();

if (isset($_POST['productId'])) {
    $id = $_POST['productId'];

    $sql = "SELECT id,feature_image FROM product WHERE id=:productId";
    $db->query($sql);
    $db->bind(':productId', $id);
    $db->execute();
    $row = $db->single();
    if ($row > 0) {
        unlink($row->feature_image);
    }

    $productImageSql = 'SELECT * FROM product_images WHERE product_id=:productId';
    $db->query($productImageSql);
    $db->bind(':productId', $id);
    $db->execute();
    $productImages = $db->resultSet();
    if ($productImages != null) {
        foreach ($productImages as $key => $productImage) {
            // echo $productImage->product_image.'<br>';
            unlink("uploads/products/" . $productImage->product_image);
        }
    }

    /*delete product record*/
    $sql = "DELETE FROM product WHERE id=:productId";
    $db->query($sql);
    $db->bind(':productId', $id);
    if ($db->execute()) {
        //       echo '<meta http-equiv="refresh" content="1;url=productIndex.php" />';
        header('location:productIndex.php');
    }
}
