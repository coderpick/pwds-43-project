<?php include_once "layout/head.php";?>

<body data-sidebar="dark">

<!-- Begin page -->
<div id="layout-wrapper">

    <?php include_once "layout/header.php";?>

    <!-- ========== Left Sidebar Start ========== -->
    <?php include_once "layout/sidebar.php";?>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Brand Edit</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">brand</a></li>
                                    <li class="breadcrumb-item active">edit</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div><!--//.row end-->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="text-right mb-2">
                                    <a href="brandIndex.php" class="btn btn-pink"><i class="fa fa-reply"></i> Back to list</a>
                                </div>
                                <?php
                                if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                                    $name            = $helper->validate($_POST['brand']);
                                    $data['status']  = $helper->validate($_POST['status']);
                                    $data['brandId'] = $_POST['brandId'];
                                    $oldImage        = $_POST['oldImage'];
                                    if (empty($name)){
                                        $error['brand_error'] ="Brand name is required";
                                    }else{
                                        $data['name'] = $name;
                                    }
                                    /*upload brand logo if change logo */
                                    $brandLogoName    = $_FILES['brand_logo']['name'];
                                    $brandLogoTmpName = $_FILES['brand_logo']['tmp_name'];
                                    if ($brandLogoName){
                                        $uploadDir   ='uploads/brand/';
                                        $ext         = strtolower(pathinfo($brandLogoName, PATHINFO_EXTENSION));
                                        $newLogoName = time().uniqid().'.'.$ext;
                                        $uploadImageName = $uploadDir.$newLogoName;
                                        /*remove old image from upload/brand folder*/
                                        unlink($oldImage);
                                        move_uploaded_file($brandLogoTmpName,$uploadImageName);
                                        $data['brand_logo'] = $uploadImageName;

                                    }else{
                                        $data['brand_logo'] = $oldImage;
                                    }

                                    if (empty( $error['brand_error'])){
                                        $updateBrand= $brand->update($data,$data['brandId']);
                                        if (isset($updateBrand)){
                                            echo  $updateBrand;
                                            echo '<meta http-equiv="refresh" content="1;url=brandIndex.php" />';
                                        }
                                    }
                                }

                                /*get url request id */
                                if (isset($_GET['id']) && !empty($_GET['id'])){
                                    $id  = $_GET['id'];
                                    $row = $brand->edit($id);
                                }
                                ?>
                                <form action="<?php echo  htmlspecialchars($_SERVER['PHP_SELF'])?>" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="brand">Brand Name</label>
                                        <input type="text" name="brand" value="<?php echo $row->brand_name??'';?>" class="form-control" id="brand">
                                        <div class="text-danger">
                                            <?php echo $error['brand_error']??''; ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="brand_logo">Brand Logo</label>
                                        <input type="file" name="brand_logo" data-default-file="<?php echo $row->brand_logo??'';?>"  data-max-file-size="1M" data-allowed-file-extensions="jpg png jpeg svg" class="form-control dropify" id="brand_logo">
                                        <div class="text-danger">
                                            <?php echo $error['brand_logo']??''; ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="status" <?php echo isset($row->status)?($row->status == true)?'checked':'':'';?>  id="inlineRadio1" value="1">
                                            <label class="form-check-label" for="inlineRadio1">Active</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="status"  <?php echo isset($row->status)?($row->status == false)?'checked':'':'';?> id="inlineRadio2" value="0">
                                            <label class="form-check-label" for="inlineRadio2">Inactive</label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="brandId" value="<?php echo $row->id??'';?>">
                                    <input type="hidden" name="oldImage" value="<?php echo $row->brand_logo??'';?>">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->


        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Skote.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-right d-none d-sm-block">
                            Design & Develop by Themesbrand
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- JAVASCRIPT -->
<?php include_once "layout/_script.php";?>
</body>

</html>