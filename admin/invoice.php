<?php include_once "layout/head.php";?>

<body data-sidebar="dark">

<!-- Begin page -->
<div id="layout-wrapper">

    <?php include_once "layout/header.php";?>

    <!-- ========== Left Sidebar Start ========== -->
    <?php include_once "layout/sidebar.php";?>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Order Details</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Order</a></li>
                                    <li class="breadcrumb-item active">details</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div><!--//.row end-->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="text-right mb-2">
                                    <a href="pendingOrder.php" class="btn btn-pink"><i class="fa fa-reply"></i> Back to list</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <?php

                                if (isset($_GET['id'])) {
                                    $orderId = $_GET['id'];
                                    $customer = $orderManagement->customerOrderInfo($orderId);
                                }
                                ?>
                              <div class="row">
                                  <div class="col-sm-6">
                                      <div class="btn-print">
                                          <a href="invoice_print.php?id=<?php echo $orderId; ?>" class="btn btn-primary"><i class="fa fa-print"></i> Print Invoice</a>
                                      </div>
                                  </div>
                                  <div class="col-sm-6 text-right">
                                      <a href="orderUpdate.php?confirm=<?php echo $orderId; ?>" class="btn btn-success">Confirm Order </a>
                                  </div>
                              </div>

                                <div class="order-summery">


                                    <div id="invoice">
                                        <div class="text-center">
                                            <h3>SmartShop.com</h3>
                                            <hr>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5> Billing Address:</h5>
                                                <p> To: <?php echo $customer->name ?? ''; ?></p>
                                                <p><strong>Phone: <?php echo $customer->mobile ?? ''; ?></strong></p>
                                                <p>Address: <?php echo $customer->shipping_add ?? ''; ?></p>

                                            </div>
                                            <div class="col-md-6 text-right">
                                                <h5>Invoice: #<?php echo $customer->order_number ?? ""; ?></h5>
                                                <p>Order Date: <?php echo $customer->order_date ?? ""; ?></p>
                                                <p><strong>Status: <?php echo $customer->status ?? ""; ?></strong></p>
                                            </div>
                                        </div>

                                        <table class="table table-bordered mt-4">
                                            <tr>
                                                <th>#</th>
                                                <th>Product</th>
                                                <th>Price</th>
                                                <th>Qty</th>
                                                <th>Total</th>
                                            </tr>
                                            <?php
                                            $orderProducts = $orderManagement->orderProduct($orderId);
                                            if ($orderProducts != null) {
                                                $totalPrice = 0;
                                                foreach ($orderProducts as $key => $orderProduct) { ?>
                                                    <tr>
                                                        <td><?php echo $key + 1; ?></td>
                                                        <td><?php echo $orderProduct->product_name; ?></td>
                                                        <td><?php echo $orderProduct->price; ?></td>
                                                        <td><?php echo $orderProduct->quantity; ?></td>
                                                        <td><?php
                                                            $total = $orderProduct->price * $orderProduct->quantity;
                                                            echo $total;
                                                            $totalPrice += $total;
                                                            ?></td>
                                                    </tr>

                                                <?php         }
                                            }
                                            ?>
                                            <tr>
                                                <td class="text-right" colspan="4">Sub Total</td>
                                                <td><?php echo $totalPrice ?? ''; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right" colspan="4">Delivery Charge</td>
                                                <td><?php echo $customer->shaping_charge??0 ?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right" colspan="4">Grand Total</td>
                                                <td><?php echo $totalPrice + $customer->shaping_charge ?? ''; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right" colspan="4">Paid Amount</td>
                                                <td><?php echo $customer->paid_amount ?? 0; ?></td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div><!--//.row end-->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->



        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Skote.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-right d-none d-sm-block">
                            Design & Develop by Coderpick
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- JAVASCRIPT -->
<?php include_once "layout/_script.php";?>
</body>

</html>