<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print invoice</title>
    <style>
        .wrapper {width: 960px;margin: 0 auto;border: 1px solid #ddd;padding-left: 15px;padding-right: 15px;}

        .title {text-align: center;background: #dede;padding: 4px;margin-left: -15px;margin-right: -15px;}


        .order-product{
            width: 100%;
            border-collapse: collapse;
            margin: 50px 0;
        }
        .order-product th,td{
            border: 1px solid #ddd;
        }
        .customer-info{
            overflow: hidden;
            margin-top: 20px;
        }
        .left {float: left;/* width: 45%; */}

        .right {float: right;}

        table.order-info{
            border-collapse: collapse;
            width: 350px;
        }

        table.order-info th,td {border: 1px solid #ddd; padding: 6px 4px}
        .order-info th{
            text-align: left;
        }


    </style>
</head>
<body>
<div class="wrapper">
    <?php
    $filepath = realpath(dirname(__FILE__));

    include_once  $filepath . "/config/config.php";
    include_once  $filepath . "/libraries/Database.php";
    include_once  $filepath . "/classes/OrderManagement.php";

    $orderManagement = new OrderManagement();


    if (isset($_GET['id'])) {
        $orderId = $_GET['id'];
        $customer = $orderManagement->customerOrderInfo($orderId);
    }

    ?>
    <div class="title">
        <h1>SmartShop</h1>
    </div>
    <div class="customer-info">
        <div class="left">
            <table class="order-info">
                <tr>
                    <th>To</th>
                    <td><?php echo $customer->name ?? ''; ?></td>
                </tr>
                <tr>
                    <th>Phone</th>
                    <td><?php echo $customer->mobile ?? ''; ?></td>
                </tr>
                <tr>
                    <th>Address</th>
                    <td><?php echo $customer->shipping_add ?? ''; ?></td>
                </tr>

            </table>
        </div>
        <div class="right">
            <table class="order-info">
                <tr>
                    <th>Invoice Number</th>
                    <td>#<?php echo $customer->order_number ?? ""; ?></td>
                </tr>
                <tr>
                    <th>Order Date:</th>
                    <td><?php echo $customer->order_date ?? ""; ?></td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td><?php echo $customer->status ?? ""; ?></td>
                </tr>

            </table>
        </div>

    </div>

    <table class="order-product">
        <tr>
            <th>#</th>
            <th>Product</th>
            <th>Price</th>
            <th>Qty</th>
            <th>Total</th>
        </tr>
        <?php
        $orderProducts = $orderManagement->orderProduct($orderId);
        if ($orderProducts != null) {
            $totalPrice = 0;
            foreach ($orderProducts as $key => $orderProduct) { ?>
                <tr>
                    <td><?php echo $key + 1; ?></td>
                    <td><?php echo $orderProduct->product_name; ?></td>
                    <td><?php echo $orderProduct->price; ?></td>
                    <td><?php echo $orderProduct->quantity; ?></td>
                    <td><?php
                        $total = $orderProduct->price * $orderProduct->quantity;
                        echo $total;
                        $totalPrice += $total;
                        ?></td>
                </tr>

            <?php         }
        }
        ?>
        <tr>
            <td style="text-align: right" colspan="4">Sub Total</td>
            <td><?php echo $totalPrice ?? ''; ?></td>
        </tr>
        <tr>
            <td style="text-align: right" colspan="4">Delivery Charge</td>
            <td><?php echo $customer->shaping_charge??0 ?></td>
        </tr>
        <tr>
            <td style="text-align: right" colspan="4">Grand Total</td>
            <td><?php echo $totalPrice + $customer->shaping_charge ?? ''; ?></td>
        </tr>
          <tr>
            <td style="text-align: right" colspan="4">Paid Amount</td>
            <td><?php echo $customer->paid_amount ?? 0; ?></td>
        </tr>
    </table>

</div>
</body>
</html>