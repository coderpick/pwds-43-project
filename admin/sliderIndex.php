<?php include_once "layout/head.php";?>

<body data-sidebar="dark">

<!-- Begin page -->
<div id="layout-wrapper">

    <?php include_once "layout/header.php";?>

    <!-- ========== Left Sidebar Start ========== -->
    <?php include_once "layout/sidebar.php";?>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Slider List</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">category</a></li>
                                    <li class="breadcrumb-item active">slider</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div><!--//.row end-->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="text-right mb-2">
                                    <a href="addSlider.php" class="btn btn-success"><i class="fa fa-plus"></i> Add New Slider</a>
                                </div>
                                <?php
                                if (isset($_GET['delete']) && !empty($_GET['delete'])){
                                    $id  = $_GET['delete'];
                                    $row = $category->delete($id);
                                    if ($row){
                                        echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                                              <strong>Success!</strong> Category Delete Successfully.
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>';
                                        echo '<meta http-equiv="refresh" content="1;url=categoryIndex.php" />';
                                    }else{
                                        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                              <strong>Whoops!</strong> Category Delete failed.
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>';
                                    }
                                }
                                ?>
                                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Image</th>
                                        <th>title</th>
                                        <th>Sub title</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $sliders = $slider->getSliders();
                                    if ($sliders){
                                        $i=1;
                                        foreach ($sliders as $slider) {?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><img src="<?php echo $slider->image; ?>" class="w-25" alt=""></td>
                                                <td><?php echo $slider->title; ?></td>
                                                <td><?php echo $slider->sub_title; ?></td>
                                                <td><?php echo $slider->price; ?></td>
                                                <td>
                                                    <?php
                                                    if ( $slider->status == true){
                                                        echo ' <span class="badge badge-success">Active</span>';
                                                    }else{
                                                        echo ' <span class="badge badge-danger">Inactive</span>';
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <a href="editSlider.php?id=<?php echo $slider->id; ?>" class="btn btn-primary"><i class="fa fa-recycle"></i> Update</a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }

                                    ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->


        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Skote.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-right d-none d-sm-block">
                            Design & Develop by Themesbrand
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- JAVASCRIPT -->
<?php include_once "layout/_script.php";?>

<?php include_once "layout/datatable.php";?>
<!--page related js file load here ...-->
<?php include_once "layout/script_activate_file.php";?>

</body>

</html>