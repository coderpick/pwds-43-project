<?php include_once "layout/head.php";?>

<body data-sidebar="dark">

<!-- Begin page -->
<div id="layout-wrapper">

    <?php include_once "layout/header.php";?>

    <!-- ========== Left Sidebar Start ========== -->
    <?php include_once "layout/sidebar.php";?>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Brand List</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">brand</a></li>
                                    <li class="breadcrumb-item active">index</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div><!--//.row end-->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="text-right mb-2">
                                    <a href="addBrand.php" class="btn btn-success"><i class="fa fa-plus"></i> Add New Brand</a>
                                </div>
                                <?php
                                if (isset($_GET['delete']) && !empty($_GET['delete'])){
                                    $id  = $_GET['delete'];
                                    $deleteBrand = $brand->delete($id);
                                    if ($deleteBrand){
                                        echo $deleteBrand;
                                        echo '<meta http-equiv="refresh" content="1;url=brandIndex.php" />';
                                    }
                                }
                                ?>
                                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Brad Name</th>
                                        <th>Slug</th>
                                        <th>Logo</th>
                                        <th>Product</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $brands = $brand->index();
                                    if ($brands !=null){
                                        $i=1;
                                        foreach ($brands as $brand){?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $brand->brand_name;?></td>
                                                <td><?php echo $brand->slug;?></td>
                                                <td width="20%">
                                                    <img src="<?php echo $brand->brand_logo;?>" class="w-25 img-thumbnail" alt="">
                                                </td>
                                                <td>0</td>
                                                <td>
                                                    <?php
                                                    if ( $brand->status ==true){
                                                        echo '<span class="badge badge-success">Active</span>';
                                                    }else{
                                                        echo '<span class="badge badge-danger">Inactive</span>';
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <a href="editBrand.php?id=<?php echo $brand->id;?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                                    <a onclick="return confirm('Are you sure to delete?')" href="?delete=<?php echo $brand->id;?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>

                                        <?php  }
                                    }
                                    ?>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->


        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Skote.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-right d-none d-sm-block">
                            Design & Develop by Themesbrand
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- JAVASCRIPT -->
<?php include_once "layout/_script.php";?>
</body>

</html>