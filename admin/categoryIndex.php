<?php include_once "layout/head.php";?>

<body data-sidebar="dark">

<!-- Begin page -->
<div id="layout-wrapper">

    <?php include_once "layout/header.php";?>

    <!-- ========== Left Sidebar Start ========== -->
    <?php include_once "layout/sidebar.php";?>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Category List</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">category</a></li>
                                    <li class="breadcrumb-item active">index</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div><!--//.row end-->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="text-right mb-2">
                                    <a href="addCategory.php" class="btn btn-success"><i class="fa fa-plus"></i> Add New Category</a>
                                </div>
                                <?php
                                if (isset($_GET['delete']) && !empty($_GET['delete'])){
                                    $id  = $_GET['delete'];
                                    $row = $category->delete($id);
                                    if ($row){
                                        echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                                              <strong>Success!</strong> Category Delete Successfully.
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>';
                                        echo '<meta http-equiv="refresh" content="1;url=categoryIndex.php" />';
                                    }else{
                                        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                              <strong>Whoops!</strong> Category Delete failed.
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>';
                                    }
                                }
                                ?>
                                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Category Name</th>
                                        <th>Slug</th>
                                        <th>Product</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $categories = $category->index();
                                    if ($categories !=null){
                                        $i=1;
                                        foreach ($categories as $category){?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $category->name;?></td>
                                                <td><?php echo $category->slug;?></td>
                                                <td>0</td>
                                                <td>
                                                    <?php
                                                    if ( $category->status ==true){
                                                        echo '<span class="badge badge-success">Active</span>';
                                                    }else{
                                                        echo '<span class="badge badge-danger">Inactive</span>';
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <a href="editCategory.php?id=<?php echo $category->id;?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                                    <a href="?delete=<?php echo $category->id;?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>

                                        <?php  }
                                    }
                                    ?>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->


        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Skote.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-right d-none d-sm-block">
                            Design & Develop by Themesbrand
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- JAVASCRIPT -->
<?php include_once "layout/_script.php";?>
</body>

</html>