<?php include_once "layout/head.php"; ?>

<body data-sidebar="dark">

    <!-- Begin page -->
    <div id="layout-wrapper">

        <?php include_once "layout/header.php"; ?>

        <!-- ========== Left Sidebar Start ========== -->
        <?php include_once "layout/sidebar.php"; ?>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="mb-0 font-size-18">Product Images</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Product</a></li>
                                        <li class="breadcrumb-item active">images</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!--//.row end-->

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="text-right mb-2">
                                        <a href="productIndex.php" class="btn btn-pink"><i class="fa fa-reply"></i> Back to list</a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <?php
                                    if (isset($_GET['id']) && !empty($_GET['id'])) {
                                        $id = $helper->validate($_GET['id']);
                                        $productImages  = $productImageObj->getProductImages($id);
                                    }

                                    /* delete image */
                                    if (isset($_GET['delete'])) {

                                        $productImageId = $_GET['delete'];

                                        $deleteProductImage = $productImageObj->deleteProductImage($productImageId);

                                        if ($deleteProductImage) {
                                            echo $deleteProductImage;
                                        }
                                    }
                                    ?>
                                    <div class="row">
                                        <div class="col-md-7">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Image</th>
                                                    <th>Action</th>
                                                </tr>
                                                <tbody>
                                                    <?php
                                                    if ($productImages != null) {
                                                        $i = 1;
                                                        foreach ($productImages as  $productImage) { ?>
                                                            <tr>
                                                                <td><?php echo $i++; ?></td>
                                                                <td>
                                                                    <img src="uploads/products/<?php echo $productImage->product_image; ?>" class="w-25" alt="">
                                                                </td>
                                                                <td>
                                                                    <a onclick="return confirm('Are you sure to delete?')" href="?id=<?php echo $id; ?>&delete=<?php echo $productImage->id; ?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                                </td>
                                                            </tr>

                                                    <?php
                                                        }
                                                    }
                                                    ?>

                                                </tbody>

                                            </table>
                                        </div>
                                        <div class="col-md-5">

                                            <?php
                                            if (isset($_POST['upload'])) {
                                                $productId = $_POST['productId'];
                                                $productImageName    = $_FILES['product_image']['name'];

                                                if (empty($productImageName)) {
                                                    $error['product_image'] = "Product image is requried";
                                                }
                                                if (empty($error['product_image'])) {

                                                    $productImageUpload = $productImageObj->uploadProductImage($_FILES, $productId);
                                                    if ($productImageUpload) {
                                                        echo $productImageUpload; ?>
                                                        <script>
                                                            function reloadIt() {
                                                                if (window.location.href.substr(-2) !== "?r") {
                                                                    window.location = window.location.href + "";
                                                                }
                                                            }
                                                            setTimeout('reloadIt()', 1000)();
                                                        </script>

                                            <?php }
                                                }
                                            }
                                            ?>

                                            <form action="" method="post" enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <label for="product_image">Product Images</label>
                                                    <input type="file" name="product_image" id="product_image" class="form-control dropify" data-max-file-size="2M" data-allowed-file-extensions="jpg png jpeg" />
                                                    <div class="text-danger">
                                                        <?php echo  $error['product_image'] ?? ""; ?>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="productId" value="<?php echo $id; ?>">
                                                <div>
                                                    <button type="submit" class="btn btn-primary" name="upload">Upload</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//.row end-->
                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->



            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <script>
                                document.write(new Date().getFullYear())
                            </script> © Skote.
                        </div>
                        <div class="col-sm-6">
                            <div class="text-sm-right d-none d-sm-block">
                                Design & Develop by Coderpick
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->


    <!-- JAVASCRIPT -->
    <?php include_once "layout/_script.php"; ?>
</body>

</html>