<?php
$filepath = realpath(dirname(__FILE__));
include_once $filepath . "/../config/config.php";
include_once $filepath . "/../libraries/Database.php";
include_once $filepath . "/../helpers/Helper.php";
include_once $filepath . "/../helpers/SlugHelper.php";
include_once $filepath . "/../helpers/Notify.php";
include_once $filepath . "/../libraries/Session.php";

spl_autoload_register(function ($class) {
    global $filepath;
    if (file_exists($filepath . '/../classes/' . $class . '.php')) {
        include_once $filepath . '/../classes/' . $class . '.php';
    }
});

if (Session::checkAdmin() == false) {
    header("Location:index.php");
}
$category        = new Category();
$subCategory     = new SubCategory();
$brand           = new Brand();
$product         = new Product();
$helper          = new Helper();
$productImageObj = new ProductImage();
$slider          = new Slider();
$orderManagement          = new OrderManagement();

?>
<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <!-- Sweet Alert-->
    <link href="assets/sweet-alert/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <!-- Summernote css -->
    <link href="assets/libs/summernote/summernote-bs4.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/dropify/css/dropify.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />

    <!-- DataTables -->
    <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap Css -->
    <link href="assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
    <style>
        p{
            margin-bottom: 0 !important;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #adb5bd8a;
        }
    </style>
</head>