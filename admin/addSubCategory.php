<?php include_once "layout/head.php";?>

<body data-sidebar="dark">

<!-- Begin page -->
<div id="layout-wrapper">

    <?php include_once "layout/header.php";?>

    <!-- ========== Left Sidebar Start ========== -->
    <?php include_once "layout/sidebar.php";?>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sub Category Create</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">sub category</a></li>
                                    <li class="breadcrumb-item active">create</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div><!--//.row end-->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="text-right mb-2">
                                    <a href="subCategoryIndex.php" class="btn btn-pink"><i class="fa fa-reply"></i> Back to list</a>
                                </div>
                                <?php
                                if ($_SERVER['REQUEST_METHOD'] === 'POST'){

                                    $categoryname    = $helper->validate($_POST['category']);
                                    $subcategoryname = $helper->validate($_POST['sub_category_name']);

                                    if (empty($categoryname)){
                                        $error['category_error'] ="Category name is required";
                                    }else{
                                        $data['category_name'] = $categoryname;
                                    }

                                    if (empty($subcategoryname)){
                                        $error['sub_category_error'] ="Sub Category name is required";
                                    }else{
                                        $data['sub_category_name'] = $subcategoryname;
                                    }

                                    if (empty( $error['category_error']) && empty($error['sub_category_error'])){

                                        $insertSubCategory = $subCategory->store($data);


                                        if ($insertSubCategory){
                                            echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                                              <strong>Success!</strong> Sub Category Insert Successfully.
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>';
                                           echo '<meta http-equiv="refresh" content="1;url=subCategoryIndex.php" />';
                                        }else{
                                            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                              <strong>Whoops!</strong> Sub Category insert failed.
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>';
                                        }
                                    }
                                }
                                ?>
                               <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
                                <div class="form-group">
                                        <label for="sub_category_name">Select Category</label>
                                         <select class="form-control" name="category">
                                             <option value="">Select Category</option>
                                             <?php 
                                                  $categories = $category->index();
                                                  if ($categories !=null){
                                                      foreach($categories as $category){?>
                                                            <option value="<?php echo $category->id;?>"><?php echo $category->name;?></option>
                                                        <?php      }
                                                  }
                                             ?>
                                         </select>
                                        <div class="text-danger">
                                            <?php echo $error['category_error']??''; ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="sub_category_name">Sub Category Name</label>
                                        <input type="text" name="sub_category_name" class="form-control" id="sub_category_name">
                                        <div class="text-danger">
                                            <?php echo $error['sub_category_error']??''; ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->



        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Skote.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-right d-none d-sm-block">
                            Design & Develop by pwds-43
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- JAVASCRIPT -->
<?php include_once "layout/_script.php";?>
</body>

</html>