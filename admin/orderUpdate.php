<?php
$filepath = realpath(dirname(__FILE__));

include_once  $filepath . "/config/config.php";
include_once  $filepath . "/libraries/Database.php";
include_once  $filepath . "/classes/OrderManagement.php";

$orderManagement = new OrderManagement();


if (isset($_GET['confirm'])) {
    $orderId = $_GET['confirm'];
    $orderConfirm = $orderManagement->orderConfirm($orderId);

    if (isset($orderConfirm)){
        echo "Order approved";

        header("location:pendingOrder.php");
    }

}

?>