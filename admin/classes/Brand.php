<?php

class Brand
{
    use SlugHelper,Notify;
    private $db;

    public function __construct()
    {
        $this->db = new  Database();
    }

    public function index()
    {
        $sql ="SELECT * FROM brand";
        $this->db->query($sql);
        $this->db->execute();
        return    $this->db->resultSet();
    }

    public function store($data,$brandLogo)
    {

        try {
            $selectBrand = "SELECT * FROM brand WHERE brand_name=:name";
            $this->db->query($selectBrand);
            $this->db->bind(':name', $data['name']);
            $this->db->execute();
            if ($this->db->rowCount() > 0) {
                return $this->erorrNotify('This brand name already exists');
            } else {
                $brandLogoName    = $_FILES['brand_logo']['name'];
                $brandLogoTmpName = $_FILES['brand_logo']['tmp_name'];
                $uploadDir ='uploads/brand/';
                $ext       = strtolower(pathinfo($brandLogoName, PATHINFO_EXTENSION));
                $newLogoName = time().uniqid().'.'.$ext;
                $uploadImageName = $uploadDir.$newLogoName;
                move_uploaded_file($brandLogoTmpName,$uploadImageName);
                $slug = $this->str_slug($data['name']);
                $sql = "INSERT INTO brand(brand_name,slug,brand_logo)VALUES(:name,:slug,:brand_logo)";
                $this->db->query($sql);
                $this->db->bind(':name', $data['name']);
                $this->db->bind(':slug', $slug);
                $this->db->bind(':brand_logo', $uploadImageName);
                if ($this->db->execute()) {
                    return $this->successNotify('Brand insert successfully');
                } else {
                    return $this->erorrNotify('Brand insert failed');
                }
            }
        }catch (PDOException $e){
            return $e->getMessage();
        }

    }

    public function edit($id)
    {

        $sql ="SELECT * FROM brand WHERE id=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return    $this->db->single();
    }
    public function update($data,$id)
    {
        $slug = $this->str_slug($data['name']);
        $sql = "UPDATE brand SET brand_name=:name,slug=:slug,brand_logo=:brandLogo,status=:status WHERE id=:id";
        $this->db->query($sql);
        $this->db->bind(':id',$id);
        $this->db->bind(':name',$data['name']);
        $this->db->bind(':slug',$slug);
        $this->db->bind(':brandLogo',$data['brand_logo']);
        $this->db->bind(':status',$data['status']);
        if ($this->db->execute()) {
            return $this->successNotify('Brand update successfully');
        } else {
            return $this->erorrNotify('Brand update failed');
        }

    }
    public function delete($id)
    {

        $sql ="SELECT * FROM brand WHERE id=:brandId";
        $this->db->query($sql);
        $this->db->bind(':brandId',$id);
        $this->db->execute();
       /* remove image form folder before delete this record */
         if($this->db->rowCount() >0){
            $row = $this->db->single();
            unlink($row->brand_logo);
         }

        $sql = "DELETE FROM brand WHERE id=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        if ($this->db->execute()){
            return  $this->successNotify('Brand Delete Successfully.');
        }else{
            return  $this->erorrNotify('Brand Delete Failed.');
        }
    }

}