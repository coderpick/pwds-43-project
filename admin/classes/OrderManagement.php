<?php


class OrderManagement
{
    private $db;

    public function __construct()
    {
        $this->db = new  Database();
    }

    public function pendingOrder()
    {
        $sql ="SELECT orders.*,customers.name as customerName FROM orders INNER JOIN customers ON orders.customer_id = customers.id WHERE status='Pending' ORDER BY orders.id DESC";

        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();

    }

     public function customerOrderInfo($id)
    {
        $sql = "SELECT orders.*,customers.name,customers.mobile,customers.shipping_add FROM orders INNER JOIN customers ON orders.customer_id = customers.id WHERE orders.id=:orderId";
        $this->db->query($sql);
        $this->db->bind(":orderId",$id);
       return $this->db->single();

    }

    public function orderProduct($id)
    {
          $sql = "SELECT * FROM order_product WHERE order_id=:orderId";
        $this->db->query($sql);
        $this->db->bind(":orderId",$id);
        $this->db->execute();
       return $this->db->resultSet();
    }

    public function orderConfirm($id)
    {
       $sql = "UPDATE orders SET status=:status WHERE id=:orderId";
       $this->db->query($sql);
       $this->db->bind(":orderId",$id);
       $this->db->bind(":status","Confirm");
       if ($this->db->execute()){
           return true;
       }else{
           return  false;
       }
    }

}