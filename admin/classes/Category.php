<?php

class Category
{
    use SlugHelper;
    private $db;

    public function __construct()
    {
        $this->db = new  Database();
    }

    public function index()
    {
        $sql ="SELECT * FROM category";
        $this->db->query($sql);
        $this->db->execute();
        return    $this->db->resultSet();
    }

    public function store($data)
    {
        $slug = $this->str_slug($data['name']);
        $sql = "INSERT INTO category(name,slug)VALUES(:name,:slug)";
        $this->db->query($sql);
        $this->db->bind(':name',$data['name']);
        $this->db->bind(':slug',$slug);
        if ($this->db->execute()){
            return true;
        }else{
            return  false;
        }

    }

    public function edit($id)
    {

        $sql ="SELECT * FROM category WHERE id=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return    $this->db->single();
    }
    public function update($data,$id)
    {
        $slug = $this->str_slug($data['name']);
        $sql = "UPDATE category SET name=:name,slug=:slug WHERE id=:id";
        $this->db->query($sql);
        $this->db->bind(':id',$id);
        $this->db->bind(':name',$data['name']);
        $this->db->bind(':slug',$slug);
        if ($this->db->execute()){
            return true;
        }else{
            return  false;
        }

    }
    public function delete($id)
    {
        $sql = "DELETE FROM category WHERE id=:id";
        $this->db->query($sql);
        $this->db->bind(':id',$id);
        if ($this->db->execute()){
            return true;
        }else{
            return  false;
        }
    }

}