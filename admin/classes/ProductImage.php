<?php 

class ProductImage{
    use Notify;
    private $db;

    public function __construct()
    {
        $this->db = new  Database();
    }

    public function getProductImages($id){

        $sql ="SELECT * FROM product_images WHERE product_id=:productId";
        $this->db->query($sql);
        $this->db->bind(':productId',$id);
        $this->db->execute();
        return    $this->db->resultSet();
    }

    public function uploadProductImage($imageFile,$productId){

        
        $productImageName       = $_FILES['product_image']['name'];
        $productImageTmpName    = $_FILES['product_image']['tmp_name'];
        
        $uploadDir  = "uploads/products/";

        $ext = strtolower(pathinfo($productImageName,PATHINFO_EXTENSION));

        $newImage = time().uniqid().'.'.$ext;

        move_uploaded_file($productImageTmpName,$uploadDir.$newImage);

        $sql ="INSERT INTO product_images(product_id,product_image) VALUES (:productId,:productImage)";
        $this->db->query($sql);
        $this->db->bind(':productId',$productId);
        $this->db->bind(':productImage',$newImage);

        if(  $this->db->execute()){
            return $this->successNotify('Product Image Uplaod Successfully');
        }else{
            return $this->erorrNotify('Product Image Upload Failed');
        
        }
    }

    public function deleteProductImage($id){

        $sql ="SELECT * FROM product_images WHERE id=:productImageId";
        $this->db->query($sql);
        $this->db->bind(':productImageId',$id);
        $this->db->execute();
       /* remove image form folder before delete this record */
         if($this->db->rowCount() >0){
            $row = $this->db->single();
            unlink("uploads/products/".$row->product_image);
         }

         /* Record delete */
         $sql ="DELETE  FROM product_images WHERE id=:productImageId";
         $this->db->query($sql);
         $this->db->bind(':productImageId',$id);
         if(  $this->db->execute()){
            return $this->successNotify('ProducImage record delete successfully');
        }else{
            return $this->erorrNotify('ProducImage  record delete Failed');
        
        }
      
    }


}
?>