<?php

class SubCategory
{
    use SlugHelper;
    private $db;

    public function __construct()
    {
        $this->db = new  Database();
    }

    public function index()
    {
        $sql ="SELECT sub_category.*, category.name as categoryName FROM sub_category
            INNER JOIN category ON sub_category.category_id = category.id";
        $this->db->query($sql);
        $this->db->execute();
        return    $this->db->resultSet();
    }

    public function store($data)
    {
       
      
        $slug = $this->str_slug($data['sub_category_name']);

        $sql = "INSERT INTO sub_category(category_id,name,slug)VALUES(:categoryId,:name,:slug)";
        $this->db->query($sql);
        $this->db->bind(':categoryId',$data['category_name']);
        $this->db->bind(':name',$data['sub_category_name']);
        $this->db->bind(':slug',$slug);
        if ($this->db->execute()){
            return true;
        }else{
            return  false;
        }

    }

    public function edit($id)
    {

        $sql ="SELECT * FROM sub_category WHERE id=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return    $this->db->single();
    }
    public function update($data,$id)
    {
      
        $slug = $this->str_slug($data['sub_category_name']);

        //$sql = "INSERT INTO sub_category(category_id,name,slug)VALUES(:categoryId,:name,:slug)";
        $sql ="UPDATE sub_category SET category_id=:catId, name=:subCatName,slug=:slug,status=:status WHERE id=:subCatId";
        $this->db->query($sql);
        $this->db->bind(':subCatId',$id);
        $this->db->bind(':catId',$data['category_name']);
        $this->db->bind(':subCatName',$data['sub_category_name']);
        $this->db->bind(':slug',$slug);
        $this->db->bind(':status',$data['status']);

        if ($this->db->execute()){
            return true;
        }else{
            return  false;
        }

    }
    public function delete($id)
    {
        $sql = "DELETE FROM sub_category WHERE id=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        if ($this->db->execute()){
            return true;
        }else{
            return  false;
        }
    }

}