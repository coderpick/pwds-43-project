<?php
class Session {
    public static function init() {
        session_start();
    }

    public static function set( $key, $value ) {
        $_SESSION[$key] = $value;
    }

    public static function get( $key ) {
        if ( isset( $_SESSION[$key] ) ) {
            return $_SESSION[$key];
        } else {
            return false;
        }
    }

    public static function checkAdmin() {
        self::init();
        if ( self::get( 'isAdmin' ) == true ) {
            //header('location:dashboard.php');
            return true;
        }else{
            return  false;
        }

    }

    public static function destroy() {
        session_destroy();
       // header( 'location:index.php' );
        /*or*/
    echo "<script>window.location='index.php'</script>";

    }
}