<?php include_once "layout/head.php";?>

<body data-sidebar="dark">

<!-- Begin page -->
<div id="layout-wrapper">

    <?php include_once "layout/header.php";?>

    <!-- ========== Left Sidebar Start ========== -->
    <?php include_once "layout/sidebar.php";?>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Product Details</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Product</a></li>
                                    <li class="breadcrumb-item active">details</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div><!--//.row end-->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="text-right mb-2">
                                    <a href="productIndex.php" class="btn btn-pink"><i class="fa fa-reply"></i> Back to list</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <?php
                                if (isset($_GET['id']) && !empty($_GET['id'])) {
                                    $id = $helper->validate($_GET['id']);
                                    $product       = $product->show($id);
                                }
                                ?>
                                <div class="media">
                                    <img src="<?php echo $product->feature_image;?>" class="mr-3 w-25" alt="...">
                                    <div class="media-body">
                                        <h4 class="mt-0"><?php echo $product->product_name;?></h4>
                                        <h5 class="text-danger font-weight-bolder">Price: <?php echo $product->price;?></h5>
                                        <h5>Stock: <?php echo $product->quantity;?></h5>
                                        <h5>Product Feature:</h5>
                                        <p>
                                            <?php echo $product->short_description;?>
                                        </p>
                                        <h5>Product Images</h5>
                                        <div class="product-image-gallery">
                                            <img src="" alt="">
                                            <img src="" alt="">
                                            <img src="" alt="">
                                            <img src="" alt="">
                                        </div>
                                    </div>
                                </div>
                                <h3>Product Details:</h3>
                                <hr>
                                <p>
                                    <?php echo htmlspecialchars_decode( $product->description);?>
                                </p>

                            </div>
                        </div>
                    </div>
                </div><!--//.row end-->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->



        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Skote.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-right d-none d-sm-block">
                            Design & Develop by Coderpick
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- JAVASCRIPT -->
<?php include_once "layout/_script.php";?>
</body>

</html>