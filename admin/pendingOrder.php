<?php include_once "layout/head.php";?>

<body data-sidebar="dark">

<!-- Begin page -->
<div id="layout-wrapper">

    <?php include_once "layout/header.php";?>

    <!-- ========== Left Sidebar Start ========== -->
    <?php include_once "layout/sidebar.php";?>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Pending Order List</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">order</a></li>
                                    <li class="breadcrumb-item active">index</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div><!--//.row end-->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="text-right mb-2">

                                </div>
                                <?php


                                ?>
                                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>

                                    <tr>
                                        <th>S/N</th>
                                        <th> Order ID</th>
                                        <th>Billing Name</th>
                                        <th>Date</th>
                                        <th>Total</th>
                                        <th>Payment Status</th>
                                        <th>Payment Method</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                        $orders = $orderManagement->pendingOrder();

                                       if ($orders !=null){
                                           $i=1;
                                           foreach ($orders as $order){?>
                                               <tr>
                                                   <td><?php echo $i++; ?></td>
                                                   <td><?php echo  $order->order_number; ?></td>
                                                   <td><?php echo  $order->customerName; ?></td>
                                                   <td><?php echo  $order->order_date; ?></td>
                                                   <td><?php echo  $order->total_amount + $order->shaping_charge; ?></td>
                                                   <td>
                                                       <?php
                                                        if ($order->payment_status == "Paid"){
                                                          echo ' <span class="badge badge-success">Paid</span>';
                                                        }else{
                                                            echo '<span class="badge badge-danger">Unpaid</span>';
                                                        }
                                                       ?>
                                                   </td>
                                                   <td><?php echo  $order->payment_type; ?></td>
                                                   <td><a href="invoice.php?id=<?php echo $order->id;?>" class="btn btn-primary">View Invoice</a></td>
                                               </tr>

                                    <?php       }
                                       }

                                    ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->


        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Skote.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-right d-none d-sm-block">
                            Design & Develop by Themesbrand
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- JAVASCRIPT -->
<?php include_once "layout/_script.php";?>
<?php include_once "layout/datatable.php"; ?>


<?php include_once "layout/script_activate_file.php"; ?>
</body>

</html>