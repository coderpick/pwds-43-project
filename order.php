<?php
include "layout/head.php";
?>

<body>

    <!-- Page Wrapper -->
    <div id="wrap" class="layout-1">
        <!-- Top bar -->
        <?php
        include "layout/top_bar.php";
        ?>
        <!-- Header -->
        <?php
        include "layout/header.php";
        ?>
        <!-- Content -->
        <div id="content">
            <?php
            if ($customer->isLoggedIn() != true) {
                echo '<script>window.location.href="login.php"</script>';
            }
            ?>
            <!-- Shopping Cart -->
            <section class="padding-bottom-60 margin-top-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="order-summery">


                                <h5>Orders</h5>
                                <hr>
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Order Number</th>
                                        <th>Order Date</th>
                                        <th>Total Item</th>
                                        <th>Total Amount</th>
                                        <th>Payment Type</th>
                                        <th>Status</th>
                                    </tr>
                                    <tbody>
                                        <?php
                                        if (isset($_SESSION['is_login']) && $_SESSION['is_login'] == true) {
                                            $customerId = $_SESSION['id'];
                                            $orders = $order->customerOrder($customerId);
                                            if ($orders != null) {
                                                foreach ($orders as $key => $order) { ?>
                                                    <tr>
                                                        <td><a href="invoice.php?id=<?php echo base64_encode($order->id); ?>" style="color: blue;">#<?php echo $order->order_number; ?></a></td>
                                                        <td><?php echo $order->order_date; ?></td>
                                                        <td><?php echo $order->total_item; ?></td>
                                                        <td><?php echo $order->total_amount + $order->shaping_charge; ?></td>
                                                        <td><?php echo $order->payment_type; ?></td>
                                                        <td><?php echo $order->status; ?></td>
                                                    </tr>
                                        <?php          }
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </div>
        <!-- End Content -->
        <!-- Footer -->
        <?php include "layout/footer.php"; ?>
        <!-- End Footer -->

        <!-- GO TO TOP  -->
        <a href="index.html#" class="cd-top"><i class="fa fa-angle-up"></i></a>
        <!-- GO TO TOP End -->
    </div>
    <!-- End Page Wrapper -->

    <!-- JavaScripts -->
    <?php include "layout/_script.php"; ?>
    <!-- page related jquery plugin load here...-->


    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <?php include "layout/_script_activate.php"; ?>

    <!-- custom js -->

</body>

</html>