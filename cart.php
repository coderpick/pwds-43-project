<?php
include "layout/head.php";
?>

<body>

<!-- Page Wrapper -->
<div id="wrap" class="layout-1">

    <!-- Top bar -->

    <?php
    include "layout/top_bar.php";
    ?>
    <!-- Header -->
    <?php
    include "layout/header.php";
    ?>
    <!-- Content -->
    <div id="content">

        <!-- Ship Process -->
        <div class="ship-process padding-top-30 padding-bottom-30">
            <div class="container">
                <ul class="row">

                    <!-- Step 1 -->
                    <li class="col-sm-3 current">
                        <div class="media-left"> <i class="flaticon-shopping"></i> </div>
                        <div class="media-body"> <span>Step 1</span>
                            <h6>Shopping Cart</h6>
                        </div>
                    </li>

                    <!-- Step 2 -->
                    <li class="col-sm-3">
                        <div class="media-left"> <i class="flaticon-business"></i> </div>
                        <div class="media-body"> <span>Step 2</span>
                            <h6>Payment Methods</h6>
                        </div>
                    </li>

                    <!-- Step 3 -->
                    <li class="col-sm-3">
                        <div class="media-left"> <i class="flaticon-delivery-truck"></i> </div>
                        <div class="media-body"> <span>Step 3</span>
                            <h6>Delivery Methods</h6>
                        </div>
                    </li>

                    <!-- Step 4 -->
                    <li class="col-sm-3">
                        <div class="media-left"> <i class="fa fa-check"></i> </div>
                        <div class="media-body"> <span>Step 4</span>
                            <h6>Confirmation</h6>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <!-- Shopping Cart -->
        <section class="shopping-cart padding-bottom-60">
            <div class="container">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Items</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Quantity</th>
                        <th class="text-center">Total Price </th>
                        <th>&nbsp; </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ( isset( $_SESSION['cart'] ) ) {
                        $totalPrice = 0;
                        foreach ( $_SESSION['cart'] as $item ) {?>
                            <!-- Item Cart -->
                            <tr>
                                <td><div class="media">
                                        <div class="media-left"> <a href="#"> <img class="img-responsive" src="admin/<?php echo $item['image'];?>" alt="" > </a> </div>
                                        <div class="media-body">
                                            <p><?php echo $item['productName']; ?></p>
                                        </div>
                                    </div></td>
                                <td class="text-center padding-top-60"><?php echo $item['price'];?></td>
                                <td class="text-center">
                                     <!-- Quantity -->
                                    <div class="quinty padding-top-20">
                                        <span><?php echo $item['qty']; ?></span>
                                    </div>
                                </td>
                                <td class="text-center padding-top-60">
                                    <?php
                                    $subTotal = $item['qty'] * $item['price'];
                                    echo $subTotal;
                                    $totalPrice += $subTotal;
                                    ?>
                                </td>
                                <td class="text-center padding-top-60"><a href="removeCartItem.php?productId=<?php echo $item['product_id']; ?>" class="remove"><i class="fa fa-close"></i></a></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>

                    </tbody>
                </table>

                <!-- Promotion -->
                <div class="promo">
                    <div class="coupen">
                        <label> Promotion Code
                            <input type="text" placeholder="Your code here">
                            <button type="submit"><i class="fa fa-arrow-circle-right"></i></button>
                        </label>
                    </div>

                    <!-- Grand total -->
                    <div class="g-totel">
                        <h5>Grand total: <span><?php echo $totalPrice??''?></span></h5>
                    </div>
                </div>

                <!-- Button -->
                <div class="pro-btn">
                    <a href="shop.php" class="btn-round btn-light">Continue Shopping</a>
                    <a href="payment.php" class="btn-round">Checkout</a> </div>
            </div>
        </section>
    </div>
    <!-- End Content -->
    <!-- Footer -->
    <?php include "layout/footer.php"; ?>
    <!-- End Footer -->

    <!-- GO TO TOP  -->
    <a href="index.html#" class="cd-top"><i class="fa fa-angle-up"></i></a>
    <!-- GO TO TOP End -->
</div>
<!-- End Page Wrapper -->

<!-- JavaScripts -->
<?php include "layout/_script.php"; ?>
<!-- page related jquery plugin load here...-->


<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<?php include "layout/_script_activate.php"; ?>

<!-- custom js -->

</body>

</html>